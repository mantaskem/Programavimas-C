//  Created by Mantas Kemėšius  on 27/11/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void GenerateArray ( int n, int A[]);
int Check ( int n, int A[]);
int main() {
    
    int n = 10, A[100], tarp;
    
    GenerateArray(n, A);
    
    printf("Random generate array: ");
    for( int i = 0 ; i < n; i++ )
    {
        printf("%d ", A[i]);
    }
    printf("\n");
    
    //------------- RIKIAVIMAS ------------------
    for ( int i = 0 ; i < n; i++ )
    {
        for ( int j = 0 ; j < n-1; j++ )
        {
            if (A[j] > A[j+1])
            {
                tarp = A[j];
                A[j] = A[j+1];
                A[j+1] = tarp;
            }
        }
    }
    //--------------------------------------------
    
    printf("Marshal array: ");
    for( int i = 0 ; i < n; i++ )
    {
        printf("%d ", A[i]);
    }
    printf("\n");
    
    if (Check(n, A) == 1 )
        printf("\nDuomenys isrikiuoti teisingai.\n");
    else
        printf("\nDuomenys isrikiuoti neteisingai.\n");
    
    return 0;
}
void GenerateArray ( int n, int A[])
{
    int r;
    srand(time(NULL));
    for ( int i = 0 ; i < n; i++ ){
        A[i] = rand() % (100 + 1 - 1) + 1;
    }
}
int Check ( int n, int A[])
{
    int yra = 0;
    int max = -9999;
    
    for ( int i = 0 ; i < n; i++ )
    {
        if (max < A[i]) max = A[i];
        
        if((i+1) == n)
            break;
        
        if (A[i] <= A[i+1])
        {
            yra = 1;
        }
        else {
            yra = 0;
            break;
        }
    }
    
    if (yra == 1 && A[n-1] == max)
        return 1;
    else
        return 0;
}
