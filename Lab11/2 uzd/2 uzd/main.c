//  Created by Mantas Kemėšius  on 28/11/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

enum BOOLEAN {FALSE, TRUE};

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void GenerateArray ( int n, int A[]);
int Check ( int n, int A[]);
void SortA ( int n, int A[]);//naivus rikiavimo algoritmas
void SortB ( int n, int A[]);//Padeda skaiciu i galutine jo pozicija ir daugiau nebetikrina jo
void SortC ( int n, int A[]);//seka ar perbėgimo metu buvo atliktas nors vienas sukeitimas.
void SortD ( int n, int A[]);//įsidėmi paskutinio sukeitimo vietą, rikiuoja tik galimai nesurikiuotą masyvo dalį
void SortE ( int n, int A[]);//gali keisti vietomis daugiau negu du elementus
void SortF ( int n, int A[]);//bidirectional bubble sort
int main() {
    //DU RIKIAVIMAI ISEJO LABAI PANASUS, BET NESUGALVOJU IR NERANDU KAIP GALIMA KITAIP PADARYT
    
    FILE *fd = fopen("rez.txt", "w");
    
    int n = 10, A[100];
    
    //TESTING-------------------------------------------------
    GenerateArray(n, A);
    
    fprintf(fd, "Masyvas: ");
    for ( int i = 0; i < n; i++ )
    {
        fprintf(fd, "%d ", A[i]);
    }
    fprintf(fd, "\n");
    
    SortF(n, A);
    
    if (Check(n, A) == 1)
    {
        fprintf(fd, "Masyvas isrikiuotas teisingai!\n");
        fprintf(fd, "Gerai isrikiuotas masyvas: ");
        for ( int i = 0; i < n; i++ )
        {
            fprintf(fd, "%d ", A[i]);
        }
        fprintf(fd, "\n");
    }
    else
    {
        fprintf(fd, "Blogai isrikiuotas masyvas: ");
        for ( int i = 0; i < n; i++ )
        {
            fprintf(fd, "%d ", A[i]);
        }
        fprintf(fd, "\n");
    }
    //TESTING END-------------------------------------------------
    
    return 0;
}
//--------------------------------------------------------
void GenerateArray ( int n, int A[])
{
    int r;
    srand(time(NULL));
    for ( int i = 0 ; i < n; i++ ){
        r = rand() % (100 + 1 - 1) + 1;
        A[i] = r;
    }
}
//--------------------------------------------------------
int Check ( int n, int A[])
{
    int yra = 0;
    int max = -9999;
    
    for ( int i = 0 ; i < n; i++ )
    {
        if (max < A[i]) max = A[i];
        
        if((i+1) == n)
            break;
        
        if (A[i] <= A[i+1])
        {
            yra = 1;
        }
        else {
            yra = 0;
            break;
        }
    }
    
    if (yra == 1 && A[n-1] == max)
        return 1;
    else
        return 0;
}
//-------------------------- SORTS -------------------------
void SortA ( int n, int A[])
{
    int tarp;
    
    for ( int i = 0 ; i < n; i++ )
    {
        for ( int j = 0 ; j < n-1; j++ )
        {
            if (A[j] > A[j+1])
            {
                tarp = A[j];
                A[j] = A[j+1];
                A[j+1] = tarp;
            }
        }
    }

}
void SortB ( int n, int A[])
{
    for ( int i = 0; i < n; i++)
    {
        int min = i;
        for ( int j = i; j < n; j++)
        {
            if( A[min] > A[j] )
            {
                min = j;
            }
        }
        int tarp = A[i];
        A[i] = A[min];//„nutempia“ į galutinę jo poziciją
        A[min] = tarp;
        //naujo perbėgimo metu savo vietoje esančio elemento nebetikrina, kadangi pirmame cikle i++;
    }
}
void SortC ( int n, int A[])
{
    int i = 0,kiek= 0, kiek2 = -1, koks, B[100];
    
    for (int z = 0; z < n; z++)
    {
        B[z] = A[z];//Susikuriam masyvo kopija, jog galetume veliau atmetineti sutvarkytas reiksmes
    }
    
    while(kiek > kiek2)//Ciklas veikia iki tol, kol neberanda mazesniu reiksmiu
    {
        int min = i;
        koks = min;
        for ( int j = i; j < n; j++)
        {
            if( A[min] > A[j] )//maziausios reiksmes ieskojimas
            {
                min = j;
            }
        }
        
        int maz = 9999, kur = 0;
        for ( int z = 0 ; z < n ; z++ )//maziausios reiksmes ieskojimas atemtimo budu
        {
            if ( maz > B[z]){
                kur= z;
                maz = B[z];
            }
        }
        B[kur] = 999;
        
        if (koks != min || maz == A[min])//Jei paimtas skaicius ir yra maziausia reiksme patikriname ar tikrai taip yra
        {
            int tarp = A[i];
            A[i] = A[min];
            A[min] = tarp;
            kiek++;
            kiek2++;
            i++;
            continue;
        }
        else
        {
            kiek2++;
            i++;
        }
    }
}
void SortD ( int n, int A[])
{
    int i = n, did;
    i--;
    
    while ( i >= 0)
    {
        did = i;//i - paskutinio sukeitimo vieta, prie kurios veliau nera griztama
        for ( int j = i; j >= 0; j--)//leidziam nuo paskutinio sukeitimo vietos iki pradzios
        {
            if(A[did] < A[j])
            {
                did = j;
            }
        }
        int tarp = A[i];
        A[i] = A[did];
        A[did] = tarp;
        i--;//Kad netikrintume, jau geroje pozicijoje esancio skaicio
    }
}
void SortE ( int n, int A[])
{
    int tarp;
    
    for ( int i = 0 ; i < n; i++ )
    {
        for ( int j = 0 ; j < n-1; j++ )
        {
            if (A[j] > A[j+1])
            {
                tarp = A[j];
                A[j] = A[j+1];
                A[j+1] = tarp;
                
                if (A[j+1] > A[j+2] && A[j+1] < A[j+3] && j+1 != n && j+2 != n && j+3 != n)
                {
                    tarp = A[j+1];
                    A[j+1] = A[j+2];
                    A[j+2] = tarp;
                }
            }
        }
    }
    
}
void SortF ( int n, int A[])
{
    int j;
    int temp;
    int limit = n;
    int st = -1;
    enum BOOLEAN swapped;
    
    limit--;
    while(st < limit)
    {
        swapped = FALSE;
        st++;
        limit--;
        for(j = st; j < limit; j++)
        {
            if(A[j] > A[j+1])
            {
                temp = A[j];
                A[j] = A[j+1];
                A[j+1] = temp;
                swapped = TRUE;
            }
        }
        
        if(!swapped)
        { return; }
        
        swapped = FALSE;
        
        for(j=limit; j >= st; j--)
        {
            if (A[j]>A[j + 1])
            {
                temp = A[j];
                A[j] = A[j+1];
                A[j+1] = temp;
                swapped = TRUE;
            }
        }
        
        if(!swapped)
        { return; }
    }
    return;
}
