//  Created by Mantas Kemėšius  on 13/11/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int * Data;
    int n;
}Stack;

void initStack(Stack *stack);
void printStack(Stack *stack);
int getStackSize(Stack *stack);
void push(Stack *stack, int value);
int top(Stack *stack);
int pop(Stack *stack);
void destroyStack(Stack *stack);

int main() {
    
    //Test
    
    Stack *stack;
    int value = 10;
    
    initStack(stack);
    
    push(stack, value);
    
    printStack(stack);
    //destroyStack(stack);
    //printStack(stack);
    
    return 0;
}

void initStack(Stack *stack)
{
    stack->Data = NULL;
    stack->n = 0;
}
void printStack(Stack *stack)
{
    for ( int i = 0 ; i < stack->n ; i++)
    {
        printf("%d ", *stack->Data+i);
    }
    printf("\n");
}
int getStackSize(Stack *stack)
{
    return stack->n;
}
void push(Stack *stack, int value)
{
    
    int *temp;
    
    if(stack->Data != NULL){
        temp=realloc(stack->Data, (stack->n+1) * sizeof(int));
        stack->Data = temp;
        *(stack->Data+stack->n)=value;
        stack->n++;
    }
    else{
        temp=(int*) malloc(sizeof(int) * stack->n+1);
        stack->Data = temp;
        *(stack->Data+stack->n)=value;
        stack->n++;
    }
}
int top(Stack *stack)
{
    if (stack->Data != NULL ){
        return *stack->Data+stack->n-1;
    }
    else
    {
        return 0;
    }
}
int pop(Stack *stack)
{
    int *temp;
    int last = *stack->Data+stack->n-1;
    
    if(stack->n > 0) {
        stack->n -= 1;
        temp=realloc(stack->Data, stack->n * sizeof(int));
        stack->Data=temp;
        return last;
    }
    else
    {
        //if data is empty return 0;
        return 0;
    }
}
void destroyStack(Stack *stack)
{
    stack->n = 0;// size = 0;
    
    realloc(stack->Data, stack->n * sizeof(int)); //atlisvinam atminti
    
    free(stack->Data); // free
}
