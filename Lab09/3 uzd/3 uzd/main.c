//  Created by Mantas Kemėšius  on 21/11/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

typedef struct{
    double x;
    double y;
} Point;

typedef struct {
    Point * Data;
    int n;
}Stack;

Point createPoint(double x, double y);

void initStack(Stack *stack);
void printStack(Stack *stack);
int getStackSize(Stack *stack);
void push(Stack *stack, double x, double y);
Point top(Stack *stack);
Point pop(Stack *stack);
void destroyStack(Stack *stack);

int main() {
    
    //Test
    
    
    Stack *stack;
    
    initStack(stack);
    
    push(stack, 1, 2);
    push(stack, 3, 4);
    push(stack, 5, 6);
    push(stack, 7, 8);
    push(stack, 9, 10);
    
    printStack(stack);
    
    
    return 0;
}
//--------------------------------------1

Point createPoint(double x, double y)
{
    Point p;
    
    p.x = x;
    p.y = y;
    
    return p;
}
//---------------------------------2
void initStack(Stack *stack)
{
    stack->Data = NULL;
    stack->n = 0;
}
void printStack(Stack *stack)
{
    double d;
    Point a;
    Point b;
    
    for(int i = 0; i < stack->n; i++) {
        printf("Kordinates: (%.1f, %.1f)\n", stack->Data[i].x, stack->Data[i].y);
        a.x = stack->Data[i].x;
        a.y = stack->Data[i].y;
        b.x = 0;
        b.y = 0;
        
        d = ((b.x - a.x)*(b.x - a.x)) + ((b.y - a.y)*(b.y - a.y));
        d = sqrt(d);
        
        printf("Ilgis nuo tasko iki centro: %.1f\n\n", d);
    }
}
int getStackSize(Stack *stack)
{
    return stack->n;
}
void push(Stack *stack, double x, double y)
{
    
    if(stack->Data != NULL){
        stack->Data = realloc(stack->Data, (stack->n+1) * sizeof(Point));
        stack->Data[stack->n].x = x;
        stack->Data[stack->n].y = y;
        stack->n++;
    }
    else{
        stack->Data=(Point*) malloc(sizeof(Point) * stack->n+1);
        stack->Data[stack->n].x = x;
        stack->Data[stack->n].y = y;
        stack->n++;
    }
}
Point top(Stack *stack)
{
    if (stack->Data != NULL ){
        return stack->Data[stack->n-1];
    }
    else
    {
        return createPoint(0, 0);//Grazinam 0-ni taska
    }
}
Point pop(Stack *stack)
{
    double lastx = stack->Data[stack->n-1].x;
    double lasty = stack->Data[stack->n-1].y;
    
    if(stack->n > 0) {
        stack->n -= 1;
        stack->Data=realloc(stack->Data, stack->n * sizeof(Point));
        return createPoint(lastx, lasty);
    }
    else
    {
        return createPoint(0, 0);//Grazinam 0-ni taska
    }
}
void destroyStack(Stack *stack)
{
    stack->n = 0;// size = 0;
    
    realloc(stack->Data, stack->n * sizeof(Point)); //atlisvinam atminti
    
    free(stack->Data);
}
