//  Created by Mantas Kemėšius  on 13/11/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <string.h>
#include <math.h>

typedef struct{
    double x;
    double y;
} Point;

void printPoint(Point p);
Point createPoint(double x, double y);
double getDistance(Point a, Point b);

int main() {
    
    getDistance(createPoint(2, -3), createPoint(-4, 5));
    
    return 0;
}
Point createPoint(double x, double y)
{
    Point p;
    
    p.x = x;
    p.y = y;
    
    return p;
}
void printPoint(Point p)
{
    printf("(%.1f, %.1f)\n", p.x, p.y);
}
double getDistance(Point a, Point b)
{
    double d;
    
    d = ((b.x - a.x)*(b.x - a.x)) + ((b.y - a.y)*(b.y - a.y));
    d = sqrt(d);
    
    return d;
}
