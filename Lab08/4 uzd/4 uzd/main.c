//  Created by Mantas Kemėšius  on 13/11/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <stdlib.h>

int SplitData (int A[], int n, int size, int **data1, int **data2);
int main() {
    
    int A[10] = {1,2,3,4,5,6,7,8,9,10};
    int n = 10;
    int size = n / 2;
    int *data1 = NULL;
    int *data2 = NULL;
    
    SplitData (A, n, size, &data1, &data2);
    
    //TEST
    /*
    if( SplitData (A, n, size, &data1, &data2) == 0)
    {
        printf("Main array: ");
        for ( int i = 0 ; i < n ; i++){
            printf("%d ", A[i]);
        }
        printf("\nFirst half of array: ");
        for ( int i = 0 ; i < size ; i++){
            printf("%d ", (data1)[i]);
        }
        printf("\nSecond half of array: ");
        for ( int i = 0 ; i < n-size ; i++){
            printf("%d ", (data2)[i]);
        }
        printf("\n");
    }
    else
        printf("Program is not working!\n");
    */
    
    return 0;
}
int SplitData (int A[], int n, int size, int **data1, int **data2)
{
    int kiek = 0;
    
    *data1 = (int *)malloc(sizeof(int)*size);
    *data2 = (int *)malloc(sizeof(int)*(n-size));
    
    if(data1 != NULL && data2 != NULL && n > 0 && size > 0)
    {
        for ( int i = 0 ; i < size; i++ )
        {
            (*data1)[i] = A[i];
        }
    
        for ( int i = size ; i < n; i++ )
        {
            (*data2)[kiek] = A[i];
            kiek++;
        }
        
        return 0;
    }
    else
    {
        return -1;
    }
    
}
