//  Created by Mantas Kemėšius  on 13/11/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * Ilgiausias (int argc, const char * argv[]);

int main(int argc, const char * argv[]) {
    
    Ilgiausias (argc, argv);
    
    return 0;
}

char * Ilgiausias (int argc, const char * argv[]){
    
    char * Pav = NULL;
    long long int len = -99999;
    int kuris = 0;
    
    for ( int i = 1 ; i < argc; i++ ){
        FILE *fp = fopen(argv[i], "rb");
        if (fp != NULL)
        {
            fseek(fp, 0, SEEK_END);
            if (len < ftell(fp))
            {
                Pav = malloc(strlen(argv[i])+1);
                len = ftell(fp);
                kuris = i;
                strcpy(Pav, argv[i]);
            }
        }
        fclose(fp);
    }
    
    printf("%s\n", Pav);
    return Pav;
}
