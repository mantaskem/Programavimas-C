//  Created by Mantas Kemėšius  on 31/10/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int createArrayA(int data1[], int size, int low, int high, int **first);
int createArrayB(int **data2, int size, int low, int high, int **first);
int main() {
    
    int data1[100];
    int *data2 = NULL;
    int *first = NULL;
    
    
    createArrayA(data1,0,0,0,&first); //A
    //createArrayB(&data2,0,0,0,&first); //B
    
    //printf("\n%d ", first);
    
    return 0;
}
int createArrayA (int data1[], int size, int low, int high, int **first){
    
    int kiek = 0;
    
    printf("Iveskite masyvo ilgi: ");
    scanf("%d", &size);
    printf("\nIveskite intervala: ");
    printf("\nNuo: ");
    scanf("%d", &low);
    printf("\nIki: ");
    scanf("%d", &high);
    
    srand(time(NULL));
    for ( int i = 0 ; i < size; i++ ){
        data1[i] = rand() % (high + 1 - low) + low;
        if ( i == 0) *first = data1[i];
        //printf("%d ", data1[i]); //Test
    }
    printf("\n");
    if (size > 0 && low < high && low > 0 && high > 0)
        return *first;
    else
        return NULL;
    
}

int createArrayB(int **data2, int size, int low, int high, int **first)
{
    
    printf("Iveskite masyvo ilgi: ");
    scanf("%d", &size);
    printf("\nIveskite intervala: ");
    printf("\nNuo: ");
    scanf("%d", &low);
    printf("\nIki: ");
    scanf("%d", &high);
    
    data2 = (int*) malloc (sizeof (int *) * size);
    
    if(data2 == NULL)
    {
        return NULL;
    }
    else{
        srand(time(NULL));
        for ( int i = 0 ; i < size; i++ ){
            *(data2+i) = rand() % (high + 1 - low) + low;
            if ( i == 0) *first = *(data2+i);
            //printf("%d ", *(data2+i)); //Test
        }
        
        return *first;
    }
    
}
