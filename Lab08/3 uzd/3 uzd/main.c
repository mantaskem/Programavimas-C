//  Created by Mantas Kemėšius  on 10/11/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <stdlib.h>

void swap2(int* x, int* y);

int main() {
    
    int x = 42;
    int y = 17;
    swap2(&x, &y);
    
    //Test
    //printf("X buvo 42, dabar: %d\nY buvo 17, dabar: %d\n", x, y);
}
void swap2(int *x, int *y) {
    
    int temp = *x;
    *x = *y;
    *y = temp;
}
