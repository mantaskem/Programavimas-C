#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

//----------------------------------------FUNKCIJOS--------------------------------------------------

void Travel(int kur, char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4], int kord[], int x, int y);
void Check (int Vieta[4][4],int Kiekis[4][4], int tarp);
void Parinkimas (int kord[], int koks);
void Board (char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4]);
int GenerateSpot();
void GenerateK(int kord[], char *box[16][16], int Lenta[], int Vieta[4][4],int x, int y, int Kiekis[4][4]);
void VartotojoSasaja();
void DrawBoard(char *box[16][16]);
void NewOne (int kord[], char *box[16][16], int Lenta[], int Vieta[4][4], int Kiekis[4][4], int kur, int x, int y);
void DeleteOld(int kord[], char *box[16][16], int Lenta[], int Vieta[4][4], int kur);
int KuriVieta(int tarp, int tarp2);
void Up(int kur, char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4], int kord[], int x, int y);
void Down(int kur, char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4], int kord[], int x, int y);
void Left(int kur, char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4], int kord[], int x, int y);
void Right(int kur, char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4], int kord[], int x, int y);
void ChangeOld(int kord[], char *box[16][16], int Lenta[], int Vieta[4][4], int x, int y, int Kiekis[4][4]);
void AddNew(int kord[], char *box[16][16], int Lenta[], int Vieta[4][4], int x, int y, int Kiekis[4][4]);
void SaveGame (char *box[16][16], int Lenta[], int Vieta[4][4], int x, int y, int Kiekis[4][4]);
void LoadGame (char *box[16][16], int Lenta[], int Vieta[4][4], int x, int y, int Kiekis[4][4]);
void SaveToTxtFile (char *box[16][16], int Lenta[], int Vieta[4][4], int x, int y, int Kiekis[4][4]);
void Highscores ();
void SaveToHighscores(char* Names);
//---------------------------------------------------------------------------------------------------

#endif // GAME_H_INCLUDED
