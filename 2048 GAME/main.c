#include "con_lib.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "game.h"
int counting = 0;
int PTS = 0;

int main(int argc, char** argv) {

    clock_t begin = clock();

    int kord[4], x, y;
    char *box[16][16];
    int Lenta[16], Vieta[4][4],Kiekis[4][4];
    char *Names;

    // Paslepia įvedamus simbolius
    con_show_echo(0);
    // Paslepia cursorių
    con_show_cursor(0);
    con_clear();

//--Sukuriama vartotojo sasaja--
    con_set_color(0, COLOR_GRAY);
    VartotojoSasaja();

//------------------------------------------
    int key = 0, p = 0,kiek = 2;
    while(1){
        // Nuskaitom visus paspaustus klavišus iš klavietūros
        while (key = con_read_key()) {
            //Up - 1, Down - 2, Left - 3, Right - 4
            if(counting == kiek)
            {
                AddNew(kord,box,Lenta,Vieta,x,y,Kiekis);
                kiek +=2;
            }

            if (key == 'w')
            {
                Travel(1, box, Lenta, Kiekis, Vieta, kord, x, y);
                DrawBoard(box);
                con_sleep(0.3);
                counting++;
                continue;
            }
            if (key == 's')
            {
                Travel(2, box, Lenta, Kiekis, Vieta, kord, x, y);
                DrawBoard(box);
                con_sleep(0.3);
                counting++;
                continue;
            }
            if (key == 'a')
            {
                Travel(3, box, Lenta, Kiekis, Vieta, kord, x, y);
                DrawBoard(box);
                con_sleep(0.3);
                counting++;
                continue;
            }
            if (key == 'd')
            {
                Travel(4, box, Lenta, Kiekis, Vieta, kord, x, y);
                DrawBoard(box);
                con_sleep(0.3);
                counting++;
                continue;
            }
            if (key == 'p')//play game
            {
                //--Pasiruosimas pries atspausdinant lenta--
                Board (box,Lenta, Kiekis, Vieta);
                GenerateK(kord,box, Lenta, Vieta,x,y, Kiekis);
                DrawBoard(box);
            }
            if(key == 'i')
            {
                SaveGame (box,Lenta,Vieta,x,y,Kiekis);
            }
            if (key == 'l')
            {
                LoadGame(box,Lenta,Vieta,x,y,Kiekis);
                DrawBoard(box);
            }
            if(key == 'e')
            {
                SaveToHighscores(Names);
                return 0;
            }
            if(key == 'h')
            {
                Highscores();
            }
            if (key == 'm')
            {
                VartotojoSasaja();
            }
            //ISEJIMAS
            else if (key == 'q')
            {
                con_clear();
                clock_t end = clock();
                double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
                int min = 0;
                double fl;
                if (time_spent / 60 > 1)
                {
                    min = time_spent / 60;
                    fl = min * 60;
                    time_spent -= fl;
                }
                printf("2048 game work %d minutes and %.0f seconds.\n\n",min, time_spent);
                return 0;
            }
        }
    }


}
void Travel(int kur, char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4], int kord[], int x, int y)
{
    int Kiekyra = 0, kelintas = 0;

    if( kur == 1)//Virsu
    {
        Up(kur,box,Lenta,Kiekis,Vieta,kord,x,y);
        DrawBoard(box);
    }
    if( kur == 2)//Apacia
    {
        Down(kur, box, Lenta, Kiekis,Vieta,kord,x,y);
        DrawBoard(box);
    }
    if( kur == 3)//Kaire
    {
        Left(kur, box, Lenta, Kiekis,Vieta,kord,x,y);
        DrawBoard(box);
    }
    if( kur == 4)//Desnia
    {
        Right(kur, box, Lenta, Kiekis,Vieta,kord,x,y);
        DrawBoard(box);
    }
    SaveToTxtFile (box,Lenta,Vieta,x,y,Kiekis);
}

//-----------------------------------------------------------------------------------------------------------------------------------

void Up(int kur, char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4], int kord[], int x, int y)
{
    int k = 0, yra = 0;

    for (int j = 0 ; j < 4; j++ )// leidziam i desne kad pereitume visus
    {
        if( Vieta[3][j] == 1)//Tikrinam ar 4 eiluteje yra pilnu langeliu
        {
            for ( int i = 2 ; i >= 0; i-- )
            {
                if( Vieta[i][j] == 0 )
                {
                    k = i;
                    yra = 1;
                }
                else
                {
                    if(Kiekis[i][j] == Kiekis[3][j])
                    {
                        yra = 0;
                        Kiekis[i][j] *=2;
                        PTS += Kiekis[i][j];
                        ChangeOld(kord,box, Lenta, Vieta,i,j, Kiekis);
                        DeleteOld(kord, box, Lenta, Vieta, KuriVieta(3, j));
                        Vieta[3][j] = 0;
                        Kiekis[3][j] = 0;
                        Lenta[KuriVieta(3,j)-1] = 0;
                        break;
                    }
                    else
                        break;
                }
            }
            if (yra == 1)
            {
                Vieta[k][j] = 1;
                Vieta[3][j] = 0;
                Kiekis[k][j] = Kiekis[3][j];
                Kiekis[3][j] = 0;
                Lenta[KuriVieta(3,j)-1] = 0;
                Lenta[KuriVieta(k,j)-1] = 1;
                NewOne(kord, box,Lenta, Vieta, Kiekis, KuriVieta(k,j), k, j);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(3,j));
            }
            yra = 0;
            k = 0;
        }//------------------------------- +++


        if( Vieta[2][j] == 1)//3 eilute
        {
            for ( int i = 1 ; i >= 0; i-- )
            {
                if( Vieta[i][j] == 0 )
                {
                    k = i;
                    yra = 1;
                }
                else
                {
                    if(Kiekis[i][j] == Kiekis[2][j])
                    {
                        yra = 0;
                        Kiekis[i][j] *=2;
                        PTS += Kiekis[i][j];
                        ChangeOld(kord,box, Lenta, Vieta,i,j, Kiekis);
                        DeleteOld(kord, box, Lenta, Vieta, KuriVieta(2, j));
                        Vieta[2][j] = 0;
                        Kiekis[2][j] = 0;
                        Lenta[KuriVieta(2,j)-1] = 0;
                        break;
                    }
                    else
                        break;
                }
            }
            if (yra == 1)
            {
                Vieta[k][j] = 1;
                Vieta[2][j] = 0;
                Kiekis[k][j] = Kiekis[2][j];
                Kiekis[2][j] = 0;
                Lenta[KuriVieta(2,j)-1] = 0;
                Lenta[KuriVieta(k,j)-1] = 1;
                NewOne(kord, box,Lenta, Vieta, Kiekis, KuriVieta(k,j), k, j);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(2,j));
            }
            yra = 0;
            k = 0;
        }//---------------------------------------------------- +++

        if( Vieta[1][j] == 1)//2 eilute
        {
            if(Vieta[0][j]==0)
            {
                Vieta[0][j] = 1;
                Vieta[1][j] = 0;
                Kiekis[0][j] = Kiekis[1][j];
                Kiekis[1][j] = 0;
                Lenta[KuriVieta(1,j)-1] = 0;
                Lenta[KuriVieta(0,j)-1] = 1;
                NewOne(kord, box,Lenta, Vieta,Kiekis, KuriVieta(1,j)-4, 0, j);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(1,j));
            }
            else if(Kiekis[0][j] == Kiekis[1][j])
            {
                con_sleep(2);
                Kiekis[0][j] *=2;
                PTS += Kiekis[0][j];
                ChangeOld(kord,box, Lenta, Vieta,0,j, Kiekis);
                DeleteOld(kord, box, Lenta, Vieta, KuriVieta(1, j));
                Vieta[1][j] = 0;
                Kiekis[1][j] = 0;
                Lenta[KuriVieta(1,j)-1] = 0;
            }
        }
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------

void Down(int kur, char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4], int kord[], int x, int y)
{
    int k = 0, yra = 0;

    for (int j = 0 ; j < 4; j++ )// leidziam i desne kad pereitume visus
    {
        if( Vieta[0][j] == 1)//Tikrinam ar 4 eiluteje yra pilnu langeliu
        {
            for ( int i = 1 ; i < 4; i++ )
            {
                if( Vieta[i][j] == 0 )
                {
                    k = i;
                    yra = 1;
                }
                else
                {
                    if(Kiekis[i][j] == Kiekis[0][j])
                    {
                        Kiekis[i][j] *=2;
                        PTS += Kiekis[i][j];
                        ChangeOld(kord,box, Lenta, Vieta, i,j, Kiekis);
                        DeleteOld(kord, box, Lenta, Vieta, KuriVieta(0, j));
                        Vieta[0][j] = 0;
                        Kiekis[0][j] = 0;
                        Lenta[KuriVieta(0,j)-1] = 0;
                        yra = 0;
                        break;
                    }
                    else
                        break;
                }
            }
            if (yra == 1)
            {
                Vieta[k][j] = 1;
                Vieta[0][j] = 0;
                Kiekis[k][j] = Kiekis[0][j];
                Kiekis[0][j] = 0;
                Lenta[KuriVieta(0,j)-1] = 0;
                Lenta[KuriVieta(k,j)-1] = 1;
                NewOne(kord, box,Lenta, Vieta, Kiekis, KuriVieta(k,j), k, j);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(0,j));
            }
            yra = 0;
            k = 0;
        }//------------------------------- +++


        if( Vieta[1][j] == 1)//3 eilute
        {
            for ( int i = 2 ; i < 4; i++ )
            {
                if( Vieta[i][j] == 0 )
                {
                    k = i;
                    yra = 1;
                }
                else
                {
                    if(Kiekis[i][j] == Kiekis[1][j])
                    {
                        yra = 0;
                        Kiekis[i][j] *=2;
                        PTS += Kiekis[i][j];
                        ChangeOld(kord,box, Lenta, Vieta,i,j, Kiekis);
                        DeleteOld(kord, box, Lenta, Vieta, KuriVieta(1, j));
                        Vieta[1][j] = 0;
                        Kiekis[1][j] = 0;
                        Lenta[KuriVieta(1,j)-1] = 0;
                        break;
                    }
                    else
                        break;
                }
            }
            if (yra == 1)
            {
                Vieta[k][j] = 1;
                Vieta[1][j] = 0;
                Kiekis[k][j] = Kiekis[1][j];
                Kiekis[1][j] = 0;
                Lenta[KuriVieta(1,j)-1] = 0;
                Lenta[KuriVieta(k,j)-1] = 1;
                NewOne(kord, box,Lenta, Vieta, Kiekis, KuriVieta(k,j), k, j);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(1,j));
            }
            yra = 0;
            k = 0;
        }//---------------------------------------------------- +++

        if( Vieta[2][j] == 1)//2 eilute
        {
            if(Vieta[3][j]==0)
            {
                Vieta[3][j] = 1;
                Vieta[2][j] = 0;
                Kiekis[3][j] = Kiekis[2][j];
                Kiekis[2][j] = 0;
                Lenta[KuriVieta(2,j)-1] = 0;
                Lenta[KuriVieta(3,j)-1] = 1;
                NewOne(kord, box,Lenta, Vieta,Kiekis, KuriVieta(2,j)+4, 3, j);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(2,j));
            }
            else if(Kiekis[3][j] == Kiekis[2][j])
            {
                con_sleep(2);
                Kiekis[3][j] *=2;
                PTS += Kiekis[3][j];
                ChangeOld(kord,box, Lenta, Vieta,3,j, Kiekis);
                DeleteOld(kord, box, Lenta, Vieta, KuriVieta(2, j));
                Vieta[2][j] = 0;
                Kiekis[2][j] = 0;
                Lenta[KuriVieta(2,j)-1] = 0;
            }
        }
    }
}
//-----------------------------------------------------------------------------------------------------------------------------------
void Left(int kur, char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4], int kord[], int x, int y)
{
    int k = 0, yra = 0;

    for ( int i = 0 ; i < 4; i++ )
    {
        if (Vieta[i][3] == 1)
        {
            for ( int j = 2 ; j >= 0; j-- )
            {
                if( Vieta[i][j] == 0 )
                {
                    k = j;
                    yra = 1;
                }
                else
                {
                    if(Kiekis[i][3] == Kiekis[i][j])
                    {
                        yra = 0;
                        Kiekis[i][j] *=2;
                        PTS += Kiekis[i][j];
                        ChangeOld(kord,box, Lenta, Vieta,i,j, Kiekis);
                        DeleteOld(kord, box, Lenta, Vieta, KuriVieta(i, 3));
                        Vieta[i][3] = 0;
                        Kiekis[i][3] = 0;
                        Lenta[KuriVieta(i,3)-1] = 0;
                        break;
                    }
                    else
                        break;
                }
            }
            if (yra == 1)
            {
                Vieta[i][k] = 1;
                Vieta[i][3] = 0;
                Kiekis[i][k] = Kiekis[i][3];
                Kiekis[i][3] = 0;
                Lenta[KuriVieta(i,3)-1] = 0;
                Lenta[KuriVieta(i,k)-1] = 1;
                NewOne(kord, box,Lenta, Vieta, Kiekis, KuriVieta(i,k), i, k);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(i,3));
            }
            yra = 0;
            k = 0;
        }//------------------------------- +++
        if(Vieta[i][2] == 1)
        {
            for ( int j = 1 ; j >= 0; j-- )
            {
                if( Vieta[i][j] == 0 )
                {
                    k = j;
                    yra = 1;
                }
                else
                {
                    if(Kiekis[i][2] == Kiekis[i][j])
                    {
                        yra = 0;
                        Kiekis[i][j] *=2;
                        PTS += Kiekis[i][j];
                        ChangeOld(kord,box, Lenta, Vieta,i,j, Kiekis);
                        DeleteOld(kord, box, Lenta, Vieta, KuriVieta(i, 2));
                        Vieta[i][2] = 0;
                        Kiekis[i][2] = 0;
                        Lenta[KuriVieta(i,2)-1] = 0;
                        break;
                    }
                    else
                        break;
                }
            }
            if (yra == 1)
            {
                Vieta[i][k] = 1;
                Vieta[i][2] = 0;
                Kiekis[i][k] = Kiekis[i][2];
                Kiekis[i][2] = 0;
                Lenta[KuriVieta(i,2)-1] = 0;
                Lenta[KuriVieta(i,k)-1] = 1;
                NewOne(kord, box,Lenta, Vieta, Kiekis, KuriVieta(i,k), i, k);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(i,2));
            }
            yra = 0;
            k = 0;
        }
        if(Vieta[i][1] == 1)
        {
            if ( Vieta[i][0] == 0)
            {
                Vieta[i][0] = 1;
                Vieta[i][1] = 0;
                Kiekis[i][0] = Kiekis[i][1];
                Kiekis[i][1] = 0;
                Lenta[KuriVieta(i,1)-1] = 0;
                Lenta[KuriVieta(i,0)-1] = 1;
                NewOne(kord, box,Lenta, Vieta,Kiekis, KuriVieta(i,0), i, 0);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(i,1));
            }
            else
            {
                if(Kiekis[i][0] == Kiekis[i][1])
                {
                    Kiekis[i][0] *= 2;
                    PTS += Kiekis[i][0];
                    ChangeOld(kord,box, Lenta, Vieta,i,0, Kiekis);
                    DeleteOld(kord, box, Lenta, Vieta, KuriVieta(i, 1));
                    Vieta[i][1] = 0;
                    Kiekis[i][1] = 0;
                    Lenta[KuriVieta(i,1)-1] = 0;
                }
            }
        }
    }
}

void Right(int kur, char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4], int kord[], int x, int y)
{
    int k = 0, yra = 0;

    for ( int i = 0 ; i < 4; i++ )
    {
        if (Vieta[i][0] == 1)
        {
            for ( int j = 1 ; j < 4; j++ )
            {
                if( Vieta[i][j] == 0 )
                {
                    k = j;
                    yra = 1;
                }
                else
                {
                    if(Kiekis[i][0] == Kiekis[i][j])
                    {
                        yra = 0;
                        Kiekis[i][j] *=2;
                        PTS += Kiekis[i][j];
                        ChangeOld(kord,box, Lenta, Vieta,i,j, Kiekis);
                        DeleteOld(kord, box, Lenta, Vieta, KuriVieta(i, 0));
                        Vieta[i][0] = 0;
                        Kiekis[i][0] = 0;
                        Lenta[KuriVieta(i,0)-1] = 0;
                        break;
                    }
                    else
                        break;
                }
            }
            if (yra == 1)
            {
                Vieta[i][k] = 1;
                Vieta[i][0] = 0;
                Kiekis[i][k] = Kiekis[i][0];
                Kiekis[i][0] = 0;
                Lenta[KuriVieta(i,0)-1] = 0;
                Lenta[KuriVieta(i,k)-1] = 1;
                NewOne(kord, box,Lenta, Vieta, Kiekis, KuriVieta(i,k), i, k);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(i,0));
            }
            yra = 0;
            k = 0;
        }//------------------------------- +++
        if(Vieta[i][1] == 1)
        {
            for ( int j = 2 ; j < 4; j++ )
            {
                if( Vieta[i][j] == 0 )
                {
                    k = j;
                    yra = 1;
                }
                else
                {
                    if(Kiekis[i][1] == Kiekis[i][j])
                    {
                        yra = 0;
                        Kiekis[i][j] *=2;
                        PTS += Kiekis[i][j];
                        ChangeOld(kord,box, Lenta, Vieta,i,j, Kiekis);
                        DeleteOld(kord, box, Lenta, Vieta, KuriVieta(i, 1));
                        Vieta[i][1] = 0;
                        Kiekis[i][1] = 0;
                        Lenta[KuriVieta(i,1)-1] = 0;
                        break;
                    }
                    else
                        break;
                }
            }
            if (yra == 1)
            {
                Vieta[i][k] = 1;
                Vieta[i][1] = 0;
                Kiekis[i][k] = Kiekis[i][1];
                Kiekis[i][1] = 0;
                Lenta[KuriVieta(i,1)-1] = 0;
                Lenta[KuriVieta(i,k)-1] = 1;
                NewOne(kord, box,Lenta, Vieta, Kiekis, KuriVieta(i,k), i, k);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(i,1));
            }
            yra = 0;
            k = 0;
        }
        if(Vieta[i][2] == 1)
        {
            if ( Vieta[i][3] == 0)
            {
                Vieta[i][3] = 1;
                Vieta[i][2] = 0;
                Kiekis[i][3] = Kiekis[i][2];
                Kiekis[i][2] = 0;
                Lenta[KuriVieta(i,2)-1] = 0;
                Lenta[KuriVieta(i,3)-1] = 1;
                NewOne(kord, box,Lenta, Vieta,Kiekis, KuriVieta(i,3), i, 3);
                DeleteOld(kord, box,Lenta, Vieta, KuriVieta(i,2));
            }
            else
            {
                if(Kiekis[i][2] == Kiekis[i][3])
                {
                    Kiekis[i][3] *= 2;
                    PTS += Kiekis[i][3];
                    ChangeOld(kord,box, Lenta, Vieta,i,3, Kiekis);
                    DeleteOld(kord, box, Lenta, Vieta, KuriVieta(i, 2));
                    Vieta[i][2] = 0;
                    Kiekis[i][2] = 0;
                    Lenta[KuriVieta(i,2)-1] = 0;
                }
            }
        }
    }
}
void ChangeOld(int kord[], char *box[16][16], int Lenta[], int Vieta[4][4], int x, int y, int Kiekis[4][4])
{
    Parinkimas (kord, KuriVieta(x,y));
    int vid_i = (kord[1]+kord[0])/2;
    int vid_j = (kord[3] + kord[2])/2;
    char *Skaiciai[]={"2","4","8","16","32","64","128","256", "512","1024","2048"};

    int tarp = 2;
    int kelint = 0;
    for ( int i = 0; i < 11; i++ )
    {
        if ( Kiekis[x][y] == tarp)
        {
            box[vid_i][vid_j] = Skaiciai[kelint];
            break;
        }
        else
        {
            tarp *=2;
            kelint++;
        }
    }
}
void AddNew(int kord[], char *box[16][16], int Lenta[], int Vieta[4][4], int x, int y, int Kiekis[4][4])
{
    int kur;
    while(1)
    {
        kur = GenerateSpot();
        if ( Lenta[kur-1] != 1 )
        {
            break;
        }
    }

    Check(Vieta,Kiekis, kur);
    Parinkimas (kord, kur);
    int vid_i = (kord[1]+kord[0])/2;
    int vid_j = (kord[3] + kord[2])/2;
    //--------------------------------

    for ( int i = kord[0]; i <= kord[1]; i++)
    {
        for ( int j = kord[2]; j <= kord[3]; j++)
        {
            if(i == vid_i && j == vid_j)
                box[i][j] = "2";
            else
                box[i][j] = "*";
        }
    }
}
void NewOne(int kord[], char *box[16][16], int Lenta[], int Vieta[4][4], int Kiekis[4][4], int kur, int x, int y)
{
    Parinkimas (kord, kur);
    int vid_i = (kord[1]+kord[0])/2;
    int vid_j = (kord[3] + kord[2])/2;
    //--------------------------------

    char *Skaiciai[]={"2","4","8","16","32","64","128","256", "512","1024","2048"};

    int tarp = 2;
    int kelint = 0;
    for ( int i = 0; i < 11; i++ )
    {
        if ( Kiekis[x][y] == tarp)
        {
            break;
        }
        else
        {
            tarp *=2;
            kelint++;
        }
    }

    for ( int i = kord[0]; i <= kord[1]; i++)
    {
        for ( int j = kord[2]; j <= kord[3]; j++)
        {
            if(i == vid_i && j == vid_j)
                box[i][j] = Skaiciai[kelint];
            else
                box[i][j] = "*";
        }
    }
}
void DeleteOld(int kord[], char *box[16][16], int Lenta[], int Vieta[4][4], int kur)
{
    Parinkimas (kord, kur);
    int vid_i = (kord[1]+kord[0])/2;
    int vid_j = (kord[3] + kord[2])/2;
    //--------------------------------

    for ( int i = kord[0]; i <= kord[1]; i++)
    {
        for ( int j = kord[2]; j <= kord[3]; j++)
        {
            box[i][j] = " ";
        }
    }
}
int KuriVieta(int tarp, int tarp2)
{
    int place;

    if (tarp == 0 && tarp2 == 0) place = 1;
    if (tarp == 0 && tarp2 == 1) place = 2;
    if (tarp == 0 && tarp2 == 2) place = 3;
    if (tarp == 0 && tarp2 == 3) place = 4;
    if (tarp == 1 && tarp2 == 0) place = 5;
    if (tarp == 1 && tarp2 == 1) place = 6;
    if (tarp == 1 && tarp2 == 2) place = 7;
    if (tarp == 1 && tarp2 == 3) place = 8;
    if (tarp == 2 && tarp2 == 0) place = 9;
    if (tarp == 2 && tarp2 == 1) place = 10;
    if (tarp == 2 && tarp2 == 2) place = 11;
    if (tarp == 2 && tarp2 == 3) place = 12;
    if (tarp == 3 && tarp2 == 0) place = 13;
    if (tarp == 3 && tarp2 == 1) place = 14;
    if (tarp == 3 && tarp2 == 2) place = 15;
    if (tarp == 3 && tarp2 == 3) place = 16;

    return place;
}
void Check (int Vieta[4][4],int Kiekis[4][4], int tarp)
{
    if (tarp == 1)
    {
        Vieta[0][0] = 1;
        Kiekis[0][0] = 2;
    }
    if (tarp == 2){
        Vieta[0][1] = 1;
        Kiekis[0][1] = 2;
    }
    if (tarp == 3)
    {
        Vieta[0][2] = 1;
        Kiekis[0][2] = 2;
    }
    if (tarp == 4)
    {
        Vieta[0][3] = 1;
        Kiekis[0][3] = 2;
    }
    if (tarp == 5)
    {
        Kiekis[1][0] = 2;
        Vieta[1][0] = 1;
    }
    if (tarp == 6)
    {
        Vieta[1][1] = 1;
        Kiekis[1][1] = 2;
    }
    if (tarp == 7)
    {
        Vieta[1][2] = 1;
        Kiekis[1][2] = 2;
    }
    if (tarp == 8)
    {
        Vieta[1][3] = 1;
        Kiekis[1][3] = 2;
    }
    if (tarp == 9)
    {
        Vieta[2][0] = 1;
        Kiekis[2][0] = 2;
    }
    if (tarp == 10)
    {
        Vieta[2][1] = 1;
        Kiekis[2][1] = 2;
    }
    if (tarp == 11)
    {
        Vieta[2][2] = 1;
        Kiekis[2][2] = 2;
    }
    if (tarp == 12)
    {
        Vieta[2][3] = 1;
        Kiekis[2][3] = 2;
    }
    if (tarp == 13)
    {
        Vieta[3][0] = 1;
        Kiekis[3][0] = 2;
    }
    if (tarp == 14)
    {
        Vieta[3][1] = 1;
        Kiekis[3][1] = 2;
    }
    if (tarp == 15)
    {
        Vieta[3][2] = 1;
        Kiekis[3][2] = 2;
    }
    if (tarp == 16)
    {
        Vieta[3][3] = 1;
        Kiekis[3][3] = 2;
    }
}
void DrawBoard(char *box[16][16])
{
    con_clear();
    printf("Points: %d\n\n", PTS);
    for ( int i = 0 ; i < 14 ; i++ ){
        for ( int j = 0; j < 14; j++ )
        {
            printf("%s ",box[i][j]);
        }
        printf("\n");
    }
    printf("\n\n1. W,S,A and D to Move\n2. L to Load previous game\n3. I to Save the game\n4. E to end and save to Highscores\n5.M to Main Menu\n6. Q to Quit the game\n");
}
int GenerateSpot()
{
    srand(time(NULL));
    int random = rand() % (16 + 1 - 1) + 1;
    return random;
}
void GenerateK(int kord[], char *box[16][16], int Lenta[], int Vieta[4][4], int x, int y, int Kiekis[4][4])
{
   //Generating random spot ----------
    int kur = GenerateSpot();
    Lenta[kur-1] = 1;
    Check(Vieta,Kiekis, kur);
    Parinkimas (kord, kur);
    int vid_i = (kord[1]+kord[0])/2;
    int vid_j = (kord[3] + kord[2])/2;
    //--------------------------------


    for ( int i = kord[0]; i <= kord[1]; i++)
    {
       for ( int j = kord[2]; j <= kord[3]; j++)
       {
          if(i == vid_i && j == vid_j)
            box[i][j] = "2";
          else{
            box[i][j] = "*";
          }
       }
    }
    while(1)
    {
        kur = GenerateSpot();
        if ( Lenta[kur-1] != 1 )
        {
            Lenta[kur-1] = 1;
            Check(Vieta,Kiekis, kur);
            Parinkimas (kord, kur);
            int vid_i = (kord[1]+kord[0])/2;
            int vid_j = (kord[3] + kord[2])/2;
            //--------------------------------

            for ( int i = kord[0]; i <= kord[1]; i++)
            {
                for ( int j = kord[2]; j <= kord[3]; j++)
                {
                    if(i == vid_i && j == vid_j)
                        box[i][j] = "2";
                    else
                        box[i][j] = "*";
                }
            }
            break;
        }
    }
}
void Board (char* box [16][16], int Lenta[], int Kiekis[4][4], int Vieta[4][4])
{
    for ( int i = 0 ; i < 14 ; i++ ){
        for ( int j = 0; j < 14; j++ ){
            box[i][j] = " ";
        }
    }
    for ( int i = 0 ; i < 14 ; i++ ){
        box[i][0] = "#";
        box[0][i] = "#";
        box[i][13] = "#";
        box[13][i] = "#";
    }
    for (int i = 0 ; i < 16; i++){
        Lenta[i] = 0;
    }

    for ( int i = 0; i < 4; i++){
        for ( int j = 0; j < 4; j++){
            Kiekis[i][j] = 0;
            Vieta[i][j] = 0;
        }
    }
}
void Parinkimas (int kord[], int koks)
{
   /* VISOS GALIMOS POZICIJOS
      ---------------
    | 1    2   3   4 |
    | 5    6   7   8 |
    | 9   10  11  12 |
    | 13  14  15  16 |
      ---------------
   */
   //1 eilute
   /*1*/ if (koks == 1) {kord[0] = 1; kord[1] = 3; kord[2] = 1; kord[3] = 3;}
   /*2*/ if (koks == 2) {kord[0] = 1; kord[1] = 3; kord[2] = 4; kord[3] = 6;}
   /*3*/ if (koks == 3) {kord[0] = 1; kord[1] = 3; kord[2] = 7; kord[3] = 9;}
   /*4*/ if (koks == 4) {kord[0] = 1; kord[1] = 3; kord[2] = 10; kord[3] = 12;}
   //2 eilute
   /*5*/ if (koks == 5) {kord[0] = 4; kord[1] = 6; kord[2] = 1; kord[3] = 3;}
   /*6*/ if (koks == 6) {kord[0] = 4; kord[1] = 6; kord[2] = 4; kord[3] = 6;}
   /*7*/ if (koks == 7) {kord[0] = 4; kord[1] = 6; kord[2] = 7; kord[3] = 9;}
   /*8*/ if (koks == 8) {kord[0] = 4; kord[1] = 6; kord[2] = 10; kord[3] = 12;}
   //3 eilute
   /*9*/ if (koks == 9) {kord[0] = 7; kord[1] = 9; kord[2] = 1; kord[3] = 3;}
   /*10*/ if (koks == 10) {kord[0] = 7; kord[1] = 9; kord[2] = 4; kord[3] = 6;}
   /*11*/ if (koks == 11) {kord[0] = 7; kord[1] = 9; kord[2] = 7; kord[3] = 9;}
   /*12*/ if (koks == 12) {kord[0] = 7; kord[1] = 9; kord[2] = 10; kord[3] = 12;}
   //4 eilute
   /*13*/ if (koks == 13) {kord[0] = 10; kord[1] = 12; kord[2] = 1; kord[3] = 3;}
   /*14*/ if (koks == 14) {kord[0] = 10; kord[1] = 12; kord[2] = 4; kord[3] = 6;}
   /*15*/ if (koks == 15) {kord[0] = 10; kord[1] = 12; kord[2] = 7; kord[3] = 9;}
   /*16*/ if (koks == 16) {kord[0] = 10; kord[1] = 12; kord[2] = 10; kord[3] = 12;}
}
void VartotojoSasaja()
{
   con_clear();
   printf(" -------------------------------------------\n");
   printf("|                                           |\n");
   printf("|           *Welcome to 2048 game!*         |\n");
   printf("|                                           |\n");
   printf(" -------------------------------------------\n");
   printf("|           [P] to Start the game           |\n");
   printf(" -------------------------------------------\n");
   printf("|         [L] to Load previous game         |\n");
   printf(" -------------------------------------------\n");
   printf("|           [H] to see Highscores           |\n");
   printf(" -------------------------------------------\n");
   printf("|                [Q] to Quite               |\n");
   printf(" -------------------------------------------\n");
}
void SaveGame (char *box[16][16], int Lenta[], int Vieta[4][4], int x, int y, int Kiekis[4][4])
{
    FILE *fd = fopen("SavedGame.bin", "wb");//Binarinio failo atidarymas

    fwrite(&PTS , 1 , sizeof(PTS) , fd );

    for ( int i = 0; i < 16; i++ )
    {
        for ( int j = 0 ; j < 16 ; j++ )
        {
            fwrite(&box[i][j] , 1 , sizeof(box[i][j]) , fd );
        }
    }
    for ( int i = 0 ; i < 16; i++)
    {
        fwrite(&Lenta[i] , 1 , sizeof(Lenta[i]) , fd );
    }

    for ( int i = 0 ; i < 4; i++ )
    {
        for ( int j= 0 ; j < 4 ; j++ )
        {
            fwrite(&Vieta[i][j] , 1 , sizeof(Vieta[i][j]) , fd );
        }
    }

    for ( int i = 0 ; i < 4; i++ )
    {
        for ( int j= 0 ; j < 4 ; j++ )
        {
            fwrite(&Kiekis[i][j] , 1 , sizeof(Kiekis[i][j]) , fd );
        }
    }

    fclose(fd);
}
void LoadGame (char *box[16][16], int Lenta[], int Vieta[4][4], int x, int y, int Kiekis[4][4])
{
    int value;
    char *value2;

    FILE *fr = fopen("SavedGame.bin", "rb");

    fread(&value, sizeof(value), 1, fr);
    PTS = value;

    for ( int i = 0; i < 16; i++ )
    {
        for ( int j = 0 ; j < 16 ; j++ )
        {
            fread(&value2, sizeof(value2), 1, fr);
            box[i][j] = value2;
        }
    }
    for ( int i = 0 ; i < 16; i++)
    {
        fread(&value, sizeof(value), 1, fr);
        Lenta[i] = value;
    }

    for ( int i = 0 ; i < 4; i++ )
    {
        for ( int j= 0 ; j < 4 ; j++ )
        {
            fread(&value, sizeof(value), 1, fr);
            Vieta[i][j] = value;
        }
    }

    for ( int i = 0 ; i < 4; i++ )
    {
        for ( int j= 0 ; j < 4 ; j++ )
        {
            fread(&value, sizeof(value), 1, fr);
            Kiekis[i][j] = value;
        }
    }

    fclose(fr);
}
void SaveToTxtFile (char *box[16][16], int Lenta[], int Vieta[4][4], int x, int y, int Kiekis[4][4])
{
    FILE *fp = fopen("Log.txt", "a");

    fprintf(fp, "-----------------------------------------------------------------\n");
    fprintf(fp,"Taskai - %d\n\nBOX:Lenta:", PTS);

    fprintf(fp,"\nLENTA:Langeliai kurie yra uzimti: ");
    for ( int i = 0 ; i < 16; i++)
    {
        fprintf(fp,"%d ", Lenta[i]);
    }
    fprintf(fp,"\n\nVIETA:Vietos, kuriose buvo tam tikri skaiciai:\n\n");
    for ( int i = 0 ; i < 4; i++ )
    {
        for ( int j= 0 ; j < 4 ; j++ )
        {
            fprintf(fp,"%d ", Vieta[i][j]);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"\nKIEKIS: Vietos, kuriose buvo tam tikri skaiciai tasku:\n\n");
    for ( int i = 0 ; i < 4; i++ )
    {
        for ( int j= 0 ; j < 4 ; j++ )
        {
            fprintf(fp,"%d ", Kiekis[i][j]);
        }
        fprintf(fp,"\n");
    }

    fprintf(fp, "\n\n-----------------------------------------------------------------\n");

    fclose(fp);
}
void Highscores ()
{
    string Name[200];
    int Taskai[1000];
    int zk = 0, value = 0;
    char value2[100];

    int cur_size = 0;

    con_clear();

    FILE *fp = fopen("Highscores.txt", "r");

    while (!feof (fp))
    {
        fscanf(fp, "%d", &Taskai[zk]);
        fscanf(fp, "%*s%n", &cur_size);
        Name[zk] = malloc((cur_size+1)*sizeof(*Name[zk]));
        zk++;
    }
    fclose(fp);

    zk++;
    fp = fopen("Highscores.txt", "r");
    for(int i = 0; i < zk; i++)
    {
        fscanf(fp, "%d", &Taskai[zk]);
        fscanf(fp, "%s", Name[i]);
    }
    zk-=2;
    fclose(fp);

    int tarp;
    char *tarp2;
    for ( int i = 0 ; i < zk ; i++ )
    {
        for (int j = 0; j < zk-1; j++ )
        {
            if (Taskai[j] < Taskai[j+1])
            {
                tarp = Taskai[j];
                Taskai[j] = Taskai[j+1];
                Taskai[j+1] = tarp;

                tarp2 = Name[j];
                Name[j] = Name[j+1];
                Name[j+1] = tarp2;
            }
        }
    }

    printf("  -------------------------\n");
    printf(" |       THE BEST OF       |\n");
    printf(" |       ~~*2048*~~        |\n");
    printf(" |                         |\n");
    printf("  -------------------------\n\n");

    for ( int i = 0 ; i < zk; i++ )
    {
        if(i <= 2)
        {
            if (i==0) printf("    * %d. %s - %d *\n", i+1, Name[i], Taskai[i]);
            if (i==1) printf("   ** %d. %s - %d **\n", i+1, Name[i], Taskai[i]);
            if (i == 2) printf("  *** %d. %s - %d ***\n", i+1, Name[i], Taskai[i]);
        }
        else{
            printf("     %d. %s - %d\n", i+1, Name[i], Taskai[i]);
        }

    }
    printf("\n\n 1.P to play the game\n 2.L to load the game\n 3.M to main menu\n 4.Q to quit the game\n");

}
void SaveToHighscores(char* Names)
{
    FILE *fp = fopen("Highscores.txt", "a");

    con_clear();
    printf("  -------------------------\n");
    printf(" |                         |\n");
    printf(" |       ~~*2048*~~        |\n");
    printf(" |                         |\n");
    printf("  -------------------------\n");
    printf(" Write your name: ");
    con_show_echo(1);
    con_show_cursor(1);
    scanf("%s", Names);
    fprintf(fp,"%d %s\n",PTS,Names);
    fclose(fp);

    printf("\n\nThank you for being together! :)\n");
}
