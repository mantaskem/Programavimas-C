//  Created by Mantas Kemėšius  on 04/12/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

//HEADER
#include "main.h"

int Priskyrimo = 0;
int Palyginimo = 0;
clock_t ti = 0;
double time_taken = 0;

void GenerateArray ( int n, int A[]);
int Check ( int n, int A[]);
void Bubble ( int n, int A[], int kuris, char *SortName[],char* Sudet[], int Palyg[],int Pris[],double Time[]);
void InsertionSort ( int n, int A[], int kuris, char *SortName[],char* Sudet[], int Palyg[],int Pris[],double Time[]);
void SelectionSort ( int n, int A[], int kuris, char *SortName[],char* Sudet[], int Palyg[],int Pris[],double Time[]);
void mergeSort(int A[], int l, int r, int kuris);
void merge(int A[], int l, int m, int r);
void Spausdinimas_1uzd ( int n, int A[], int B[]);
void Spausdinimas_2uzd (int kuris, char *SortName[], char *Sudet[], int Palyg[],int Pris[],double Time[]);
int main ()
{
    int n = 10, A[200], B[100], Pris[10], Palyg[10],kuris;
    double Time[10];
    char * SortName[10],*Sudet[10];
    
    GenerateArray(n, A);
    
    for ( int i = 0; i < n; i++ )
    {
        B[i] = A[i];
    }
    
//---------------------------SORTS-----------------------------------
    
//----------------- Bubble ---------------------
    
    Bubble(n, A, kuris,SortName,Sudet,Palyg,Pris,Time);
    
//----------------- Insertion ---------------------
    
    for ( int i = 0; i < n; i++ )
    {
        A[i] = B[i];
    }
    
    
    InsertionSort(n, A, kuris,SortName,Sudet,Palyg,Pris,Time);
    
//----------------- Selection ---------------------
    
    for ( int i = 0; i < n; i++ )
    {
        A[i] = B[i];
    }
    
    
    SelectionSort(n, A, kuris,SortName,Sudet,Palyg,Pris,Time);
    
//----------------- Merge ---------------------
    
    for ( int i = 0; i < n; i++ )
    {
        A[i] = B[i];
    }
    
    //----------Merge sort-------------
    
    ti = clock();
    mergeSort(A, 0, n, kuris);
    for ( int i = 0 ; i < n+1; i++){//Pertraukimas vienu elementu **ATKOMENTUOTI VISADA**
        A[i] = A[i+1];
    }
    time_taken = ti / (double)CLOCKS_PER_SEC;
    
    SortName[3] = "Merge sort";
    Pris[3] = Priskyrimo;
    Palyg[3] = Palyginimo;
    Priskyrimo = 0;
    Palyginimo = 0;
    Time[3] = time_taken;
    Sudet[3] = "O(n log n)";
    //---------------------------------
    
//-----------------------------------------------
    
//-------------------------SORTS END-----------------------------------
    
/********************************* SPAUSDINIMAS, TESTAVIMAS ***********************************************/
    
//------------ 1 uzduoties spausdinimas ------------------------
    Spausdinimas_1uzd ( n, A, B);
//--------------------------------------------------------------
    
    char *tarp;
    double tarp2;
    int tarp3;
    int sk = 4;
    for (int i = 0 ; i < sk; i++ )
    {
        for ( int j = 0 ; j < sk-1; j++)
        {
            if (Time[j] >= Time[j+1] )
            {
                if(Time[j] != Time[j+1])
                {
                    tarp = SortName[j];
                    SortName[j] = SortName[j+1];
                    SortName[j+1] = tarp;
                
                    tarp2 = Time[j];
                    Time[j] = Time[j+1];
                    Time[j+1] = tarp2;
                
                    tarp3 = Palyg[j];
                    Palyg[j] = Palyg[j+1];
                    Palyg[j+1] = tarp3;
                
                    tarp3 = Pris[j];
                    Pris[j] = Pris[j+1];
                    Pris[j+1] = tarp3;
                
                    tarp = Sudet[j];
                    Sudet[j] = Sudet[j+1];
                    Sudet[j+1] = tarp;
                }
                else if(Pris[j]+Palyg[j] > Pris[j+1]+Palyg[j+1])
                {
                    tarp = SortName[j];
                    SortName[j] = SortName[j+1];
                    SortName[j+1] = tarp;
                    
                    tarp2 = Time[j];
                    Time[j] = Time[j+1];
                    Time[j+1] = tarp2;
                    
                    tarp3 = Palyg[j];
                    Palyg[j] = Palyg[j+1];
                    Palyg[j+1] = tarp3;
                    
                    tarp3 = Pris[j];
                    Pris[j] = Pris[j+1];
                    Pris[j+1] = tarp3;
                    
                    tarp = Sudet[j];
                    Sudet[j] = Sudet[j+1];
                    Sudet[j+1] = tarp;
                }
            }
        }
    }
    
    
//------------ 2 uzduoties spausdinimas ------------------------
    Spausdinimas_2uzd (kuris, SortName, Sudet, Palyg, Pris, Time);
//--------------------------------------------------------------

return 0;
}
//--------------------------------------------------------
void GenerateArray ( int n, int A[])
{
    int r;
    srand(time(NULL));
    for ( int i = 0 ; i < n; i++ ){
        r = rand() % (1000 + 1 - 1) + 1;
        A[i] = r;
    }
}
//--------------------------------------------------------
int Check ( int n, int A[])
{
    int yra = 0;
    int max = -9999;
    
    for ( int i = 0 ; i < n; i++ )
    {
        if (max < A[i]) max = A[i];
        
        if((i+1) == n)
            break;
        
        if (A[i] <= A[i+1])
        {
            yra = 1;
        }
        else {
            yra = 0;
            break;
        }
    }
    
    if (yra == 1 && A[n-1] == max)
        return 1;
    else
        return 0;
}
//---------------SORTS-----------------
void Bubble ( int n, int A[], int kuris, char *SortName[],char* Sudet[], int Palyg[],int Pris[],double Time[])
{
    kuris = 0;
    int tarp;
    ti = clock();
    for ( int i = 0 ; i < n; i++ )
    {
        for ( int j = 0 ; j < n-1; j++ )
        {
            if (A[j] > A[j+1])
            {
                Palyginimo++;
                Priskyrimo+=3;
                tarp = A[j];
                A[j] = A[j+1];
                A[j+1] = tarp;
            }
        }
    }
    time_taken = ti / (double)CLOCKS_PER_SEC;
    
    SortName[0] = "Bubble sort";
    Pris[0] = Priskyrimo;
    Palyg[0] = Palyginimo;
    Priskyrimo = 0;
    Palyginimo = 0;
    Time[0] = time_taken;
    Sudet[0] = "О(n2)";
}
void InsertionSort ( int n, int A[], int kuris, char *SortName[],char* Sudet[], int Palyg[],int Pris[],double Time[])
{
    kuris = 1;
    ti = clock();
    for (int i = 1 ; i <= n - 1; i++) {
        int d = i;
        while ( d > 0 && A[d] < A[d-1]) {
            int t = A[d];
            A[d] = A[d-1];
            A[d-1] = t;
            d--;
            Priskyrimo+=3;
        }
    }
    time_taken = ti / (double)CLOCKS_PER_SEC;
    
    SortName[1] = "Insertion sort";
    Pris[1] = Priskyrimo;
    Palyg[1] = Palyginimo;
    Priskyrimo = 0;
    Palyginimo = 0;
    Time[1] = time_taken;
    Sudet[1] = "O(n lg n) ";
}
void SelectionSort ( int n, int A[], int kuris, char *SortName[],char* Sudet[], int Palyg[],int Pris[],double Time[])
{
    kuris = 2;
    int swap;
    
    ti = clock();
    for (int j = 0; j < n-1; j++)
    {
        int iMin = j;
        Priskyrimo++;
        for ( int i = j+1; i < n; i++) {
            if (A[i] < A[iMin])
            {
                iMin = i;
                Palyginimo++;
                Priskyrimo++;
            }
        }
        
        if(iMin != j)
        {
            Palyginimo++;
            swap = A[iMin];
            A[iMin] = A[j];
            A[j] = swap;
            Priskyrimo+=3;
        }
    }
    time_taken = ti / (double)CLOCKS_PER_SEC;
    
    SortName[2] = "Selection sort";
    Pris[2] = Priskyrimo;
    Palyg[2] = Palyginimo;
    Priskyrimo = 0;
    Palyginimo = 0;
    Time[2] = time_taken;
    Sudet[2] = "O(n2)";
}
//--------------------------------------
void merge(int A[], int l, int m, int r)
{
    int i, j, k;
    int n1 = m - l +1;
    int n2 =  r - m;
    
    int L[n1], R[n2];
    
    for (i = 0; i < n1; i++)
        L[i] = A[l + i];
    for (j = 0; j < n2; j++)
        R[j] = A[m + 1+ j];
    
    Priskyrimo+=4;
    
    
    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2)
    {
        if (L[i] <= R[j])
        {
            Palyginimo++;
            A[k] = L[i];
            i++;
            Priskyrimo++;
        }
        else
        {
            Palyginimo++;
            A[k] = R[j];
            Priskyrimo++;
            j++;
        }
        k++;
    }
    
    while (i < n1)
    {
        A[k] = L[i];
        Priskyrimo++;
        i++;
        k++;
    }
    
    while (j < n2)
    {
        A[k] = R[j];
        Priskyrimo++;
        j++;
        k++;
    }
}

void mergeSort(int A[], int l, int r, int kuris)
{
    kuris = 3;
    ti = clock();
    if (l < r)
    {
        Palyginimo++;
        int m = l+(r-l)/2;
        Priskyrimo++;
        
        mergeSort(A, l, m, kuris);
        mergeSort(A, m+1, r,kuris);
        
        merge(A, l, m, r);
    }
    time_taken = ti / (double)CLOCKS_PER_SEC;
}

//------------- SPAUSDINIMAS ------------------------------------------------------

void Spausdinimas_1uzd ( int n, int A[], int B[])
{
    printf("Masyvas: ");
    for ( int i = 0; i < n; i++ )
    {
        printf("%d ", A[i]);
    }
    printf("\n");
    
    if (Check(n, A) == 1)
    {
        printf("Masyvas isrikiuotas teisingai!\n");
        printf("Gerai isrikiuotas masyvas: ");
        for ( int i = 0; i < n; i++ )
        {
            printf("%d ", A[i]);
        }
        printf("\n");
    }
    else
    {
        printf("Blogai isrikiuotas masyvas: ");
        for ( int i = 0; i < n; i++ )
        {
            printf("%d ", A[i]);
        }
        printf("\n");
    }
    
}
void Spausdinimas_2uzd (int kuris, char *SortName[], char *Sudet[], int Palyg[],int Pris[],double Time[])
{
    printf("Pavadinimas  |  Palyginimo sakiniu  |  Priskyrimo sakiniu  |  Laikas  |  Sudetingumas  |\n");
    for (int i = 0 ; i < 4; i++ ){
        if(i == 0) printf("%s            %d                     %d             %f       %s       \n",SortName[i],Palyg[i], Pris[i], Time[i], Sudet[i]);
        if(i == 1) printf("%s         %d                      %d             %f       %s       \n",SortName[i],Palyg[i], Pris[i], Time[i], Sudet[i]);
        if(i == 2) printf("%s         %d                     %d             %f       %s       \n",SortName[i],Palyg[i], Pris[i], Time[i], Sudet[i]);
        if(i == 3) printf("%s             %d                     %d             %f       %s       \n",SortName[i],Palyg[i], Pris[i], Time[i], Sudet[i]);
    }
    
}
