	.file	"file.c"
	.section .rdata,"dr"
LC0:
	.ascii "wb\0"
	.text
	.globl	_saveToFile
	.def	_saveToFile;	.scl	2;	.type	32;	.endef
_saveToFile:
LFB10:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$LC0, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -16(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$4, 8(%esp)
	movl	$1, 4(%esp)
	leal	16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fwrite
	movl	$0, -12(%ebp)
	jmp	L2
L3:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	12(%ebp), %eax
	addl	%eax, %edx
	movl	-16(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$4, 8(%esp)
	movl	$1, 4(%esp)
	movl	%edx, (%esp)
	call	_fwrite
	addl	$1, -12(%ebp)
L2:
	movl	16(%ebp), %eax
	cmpl	%eax, -12(%ebp)
	jl	L3
	movl	_saveCount, %eax
	addl	$1, %eax
	movl	%eax, _saveCount
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE10:
	.section .rdata,"dr"
LC1:
	.ascii "rb\0"
	.text
	.globl	_loadFromFile
	.def	_loadFromFile;	.scl	2;	.type	32;	.endef
_loadFromFile:
LFB11:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$LC1, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -16(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$1, 8(%esp)
	movl	$4, 4(%esp)
	leal	16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fread
	movl	$0, -12(%ebp)
	jmp	L5
L6:
	movl	-16(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$1, 8(%esp)
	movl	$4, 4(%esp)
	leal	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_fread
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	12(%ebp), %eax
	addl	%eax, %edx
	movl	-20(%ebp), %eax
	movl	%eax, (%edx)
	addl	$1, -12(%ebp)
L5:
	movl	16(%ebp), %eax
	cmpl	%eax, -12(%ebp)
	jl	L6
	movl	_loadCount, %eax
	addl	$1, %eax
	movl	%eax, _loadCount
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE11:
	.ident	"GCC: (GNU) 5.3.0"
	.def	_fopen;	.scl	2;	.type	32;	.endef
	.def	_fwrite;	.scl	2;	.type	32;	.endef
	.def	_fclose;	.scl	2;	.type	32;	.endef
	.def	_fread;	.scl	2;	.type	32;	.endef
