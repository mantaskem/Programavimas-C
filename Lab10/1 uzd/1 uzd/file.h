#ifndef file_h
#define file_h

extern int saveCount;
extern int loadCount;

void loadFromFile (char *file, int m[], int size);
void saveToFile (char *file, int m[], int size);

#endif
