//  Created by Mantas Kemėšius  on 20/11/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "file.h"

int saveCount = 0;
int loadCount = 0;

void fillArray ( int size, int m[]);
void printArray ( int size, int m[]);
int main ()
{
    //---------------------------------- 1 a.
    /*
    int m1[100], n1 = 10;
    int m2[100], n2 = n1; // VEIKIA TIK KAI ZINO KOKS ILGIS!!
    int yra = 1;
     
    fillArray(n1, m1);
    printArray(n1, m1);
    saveToFile ( m1, n1);
    loadFromFile ( m2, n2);
    printArray(n2, m2);
    
    for ( int i = 0 ; i < n1; i++ ){
        if ( m1[i] != m2[i] ) {
            yra = 0;
            break;
        }
    }
    
    if (yra == 1) printf("\nAll good\n");
    else printf("\nSomething’s wrong\n");
    */
    
    //----------------------------------- 1 b.
    
    int m1[100], n1 = 10;
    int m2[100], n2 = 10;
    int m3[100], n3 = 10;
    
    fillArray(n1, m1);
    fillArray(n2, m2);
    fillArray(n3, m3);
    saveToFile ("data.bin", m1, n1);
    saveToFile ("data.bin", m3, n3);
    loadFromFile ("data.bin", m1, n1);
    saveToFile ("data2.bin", m2, n2);
    loadFromFile ("data2.bin", m3, n3);
    loadFromFile ("data.bin", m2, n2);
    
    if (saveCount == 3 && loadCount == 3 )
        printf("All good\n");
    else
        printf("Something’s wrong %d %d\n", saveCount, loadCount);
    
    //----------------------------------- 1 c.
    
    return 0;
}
void fillArray ( int size, int m[])
{
    int r;
    srand(time(NULL));
    for ( int i = 0 ; i < size; i++ ){
        r = rand() % (100 + 1 - 1) + 1;
        m[i] = r;
    }

}
void printArray ( int size, int m[])
{
    printf("Sugeneruotas masyvas: ");
    for ( int i = 0 ; i < size; i++ ){
        printf("%d ", m[i]);
    }
    printf("\n");
}
