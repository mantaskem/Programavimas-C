	.file	"main.c"
	.globl	_saveCount
	.bss
	.align 4
_saveCount:
	.space 4
	.globl	_loadCount
	.align 4
_loadCount:
	.space 4
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
LC0:
	.ascii "data.bin\0"
LC1:
	.ascii "data2.bin\0"
LC2:
	.ascii "All good\0"
LC3:
	.ascii "Something\342\200\231s wrong %d %d\12\0"
	.text
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB13:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$1232, %esp
	call	___main
	movl	$10, 1228(%esp)
	movl	$10, 1224(%esp)
	movl	$10, 1220(%esp)
	leal	820(%esp), %eax
	movl	%eax, 4(%esp)
	movl	1228(%esp), %eax
	movl	%eax, (%esp)
	call	_fillArray
	leal	420(%esp), %eax
	movl	%eax, 4(%esp)
	movl	1224(%esp), %eax
	movl	%eax, (%esp)
	call	_fillArray
	leal	20(%esp), %eax
	movl	%eax, 4(%esp)
	movl	1220(%esp), %eax
	movl	%eax, (%esp)
	call	_fillArray
	movl	1228(%esp), %eax
	movl	%eax, 8(%esp)
	leal	820(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_saveToFile
	movl	1220(%esp), %eax
	movl	%eax, 8(%esp)
	leal	20(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_saveToFile
	movl	1228(%esp), %eax
	movl	%eax, 8(%esp)
	leal	820(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_loadFromFile
	movl	1224(%esp), %eax
	movl	%eax, 8(%esp)
	leal	420(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC1, (%esp)
	call	_saveToFile
	movl	1220(%esp), %eax
	movl	%eax, 8(%esp)
	leal	20(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC1, (%esp)
	call	_loadFromFile
	movl	1224(%esp), %eax
	movl	%eax, 8(%esp)
	leal	420(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_loadFromFile
	movl	_saveCount, %eax
	cmpl	$3, %eax
	jne	L2
	movl	_loadCount, %eax
	cmpl	$3, %eax
	jne	L2
	movl	$LC2, (%esp)
	call	_puts
	jmp	L3
L2:
	movl	_loadCount, %edx
	movl	_saveCount, %eax
	movl	%edx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	$LC3, (%esp)
	call	_printf
L3:
	movl	$0, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE13:
	.globl	_fillArray
	.def	_fillArray;	.scl	2;	.type	32;	.endef
_fillArray:
LFB14:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$0, (%esp)
	call	_time
	movl	%eax, (%esp)
	call	_srand
	movl	$0, -12(%ebp)
	jmp	L6
L7:
	call	_rand
	movl	%eax, %ecx
	movl	$1374389535, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$5, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	imull	$100, %eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$1, %eax
	movl	%eax, -16(%ebp)
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	12(%ebp), %eax
	addl	%eax, %edx
	movl	-16(%ebp), %eax
	movl	%eax, (%edx)
	addl	$1, -12(%ebp)
L6:
	movl	-12(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	L7
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE14:
	.section .rdata,"dr"
LC4:
	.ascii "Sugeneruotas masyvas: \0"
LC5:
	.ascii "%d \0"
	.text
	.globl	_printArray
	.def	_printArray;	.scl	2;	.type	32;	.endef
_printArray:
LFB15:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$LC4, (%esp)
	call	_printf
	movl	$0, -12(%ebp)
	jmp	L9
L10:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	12(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	$LC5, (%esp)
	call	_printf
	addl	$1, -12(%ebp)
L9:
	movl	-12(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	L10
	movl	$10, (%esp)
	call	_putchar
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE15:
	.ident	"GCC: (GNU) 5.3.0"
	.def	_saveToFile;	.scl	2;	.type	32;	.endef
	.def	_loadFromFile;	.scl	2;	.type	32;	.endef
	.def	_puts;	.scl	2;	.type	32;	.endef
	.def	_printf;	.scl	2;	.type	32;	.endef
	.def	_time;	.scl	2;	.type	32;	.endef
	.def	_srand;	.scl	2;	.type	32;	.endef
	.def	_rand;	.scl	2;	.type	32;	.endef
	.def	_putchar;	.scl	2;	.type	32;	.endef
