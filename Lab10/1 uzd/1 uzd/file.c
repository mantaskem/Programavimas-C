//  Created by Mantas Kemėšius  on 20/11/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include "file.h"

void saveToFile (char *file, int m[], int size)
{
    
    FILE *fd = fopen(file, "wb");

    
    fwrite(&size , 1 , sizeof(size) , fd );
    
    for ( int i = 0; i < size; i++ )
    {
        fwrite(&m[i] , 1 , sizeof(m[i]) , fd );
    }
    saveCount++;
    fclose(fd);
    
}
void loadFromFile (char *file, int m[], int size)
{
    int value;
    
    FILE *fr = fopen(file, "rb");
    
    fread(&size, sizeof(size), 1, fr);
    
    
    for ( int i = 0; i < size; i++ )
    {
        fread(&value, sizeof(value), 1, fr);
        m[i] = value;
    }
    
    loadCount++;
    
    fclose(fr);
}
