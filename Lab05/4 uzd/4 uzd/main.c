//  Created by Mantas Kemėšius  on 23/10/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include<stdio.h>

int main()
{
    int n, c = 2;
    int x = 1, ii = 0, A[1000], B[1000], jj = 0, yra = 0;
    
    printf("Veskite begaline teigiamu skaiciu seka, noredami ja uzbaigti iveskite neigiama skaiciu:\n");
    
    while (x > 0){
        scanf("%d", &x);
        A[ii] = x;
        ii++;
    }
    ii--;
    
    for ( int i = 0 ; i < ii; i++) {
        for ( int j = 0; j < jj; j++){
            if( A[i] == B[j] )
            {
                yra = 1;
                break;
            }
        }
        if (yra == 0 )
        {
            B[jj] = A[i];
            jj++;
        }
        else yra = 0;
    }
    
    printf("Prime numbers: ");
    for ( int i = 0 ; i < jj; i++ )
    {
        n = B[i];
        for ( c = 2 ; c <= n - 1 ; c++ )
        {
            if ( n%c == 0 )
            {
                break;
            }
        }
    
        if ( c == n )
            printf("%d ", n);
    
    }
    printf("\n");

    return 0;
}
