//  Created by Mantas Kemėšius  on 23/10/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>

int main() {
    
    int s, n, ii = 0, A[1000], tarp = 0;
    
    printf("Iveskite 2 skaicius:\n");
    scanf("%d%d", &s, &n);
    
    printf("Iveskite %d teigiamas reiksmes:\n", n);
    for ( int i = 0 ; i < n; i++ )
    {
        while (1)
        {
            scanf("%d", &A[ii]);
            if ( A[ii] > 0 )
            {
                ii++;
                break;
            }
            else printf("Jus ivedete neigiama reiksme, pakartokite ivedima:\n");
        }
    }
    
    printf("Programa ras visu jusu ivestu skaiciu sandaug jei ji yra lygi %d:\n", s);
    
    for ( int i = 0 ; i < ii; i++){
        for ( int j = 0; j < ii; j++ ){
            if (A[i] * A[j] == s )
            {
                printf("(%d, %d)\n", A[i], A[j]);
                tarp = 1;
            }
        }
    }
    
    if (tarp == 0) printf("\nTokiu skaiciu nera, su kuriais galima gauti teisinga sandauga.\n");
    
    return 0;
}
