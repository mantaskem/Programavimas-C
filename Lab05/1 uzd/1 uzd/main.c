//  Created by Mantas Kemėšius  on 23/10/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>

int main() {
    
    int A[10] = {0,0,0,0,0,0,0,0,0,0};
    int x,y;
    
    printf("Pradinis masyvas:\n");
    for ( int i = 0; i < 10; i++ )
    {
        printf("%d ", A[i]);
    }
    
    printf("\n");
    
    A[0] = 1;
    A[3] = 2;
    A[9] = 3;
    
    for ( int i = 2; i < 10; i++)
    {
        A[i] = A[i+1];
    }
    
    for ( int i = 8; i > 0; i--){
        A[i+1] = A[i];
    }
    
    A[6] = 4;
    
    printf("Po pakeitimu:\n");
    for ( int i = 0; i < 10; i++ )
    {
        printf("%d ", A[i]);
    }
    
    printf("\n\n");
    
    printf("Iveskite du naturalius skaicius x ir y bei iterpsime i indeksa x elementa y: \n");
    scanf("%d %d", &x, &y);
    
    A[x] = y;
    printf("\n\n");
    for ( int i = 0 ; i < 10; i++ ){
        printf ("%d ", A[i]);
    }
    printf("\n");
    
    printf("Iveskite naturalu skaiciu nuo 0 iki 9, kuri skaiciu noretumete pasalinti: \n");
    scanf("%d", &x);
    
    for ( int i = x; i < 10; i++ ){
        A[i] = A[i+1];
    }
    
    printf("\n\n");
    for ( int i = 0 ; i < 9; i++ ){
        printf ("%d ", A[i]);
    }
    printf("\n");
    
    printf("Iveskite du naturalius skaicius, masyvo indeksa x ir norima elementa y, x-ojoje vietoje iterpsim skaiciu y: \n");
    scanf("%d %d", &x, &y);
    
    for ( int i = 9; i >= x; i-- )
    {
        A[i+1] = A[i];
    }
    A[x] = 100;
    
    for ( int i = 0 ; i < 10; i++ ){
        printf ("%d ", A[i]);
    }
    printf("\n");
    
    return 0;
}
