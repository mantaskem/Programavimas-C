//  Created by Mantas Kemėšius  on 23/10/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.
//

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int randomRange(int min, int max)
{
    return ( rand() % ( max - min ) ) + min;
}

int main(void)
{
    int A[1000];
    int a, b, c;
    
    printf("Iveskite a, b, c naturalius skaicius ir atspausdinsim c atsitiktinai sugeneruotų reiksmiu, kuriu kiekviena priklauso intervalui [a; b]\n");
    scanf("%d %d %d", &a, &b, &c);
    
    int i=0;
    srand(time(NULL));
    while(i++!=c)
        A[i] = randomRange(a,b);
    
    for ( int i = 0 ; i < c; i++)
    {
        printf("%d ", A[i]);
    }
    printf("\n");
    
    
    return 0;
}
