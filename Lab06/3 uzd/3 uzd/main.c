////  Created by Mantas Kemėšius  on 23/10/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
    int a, b, c, x = 0;
    char str[200];
    int yra = 0;
    printf("Irasykite 3 skaicius, isskirdami kabletaskiu a;b;c\n");
    scanf("%d;%d;%d", &a, &b, &c);
    
    if((a > b && a < c) || (a < b && a > c))
    {
        x = a;
    }
    if((b > a && b < c) || (b < a && b > c))
    {
        x = b;
    }
    if((c > b && c < a) || (c < b && c > a))
    {
        x= c;
    }
    if(a==b || b == c || a==c)
    {
        printf("Skaiciai neturi vidurines reiksmes\n");
    }
    
    printf("Iveskite failo pavadinima, gale turi but .txt\n");
    
    scanf ("%199s",str);
    
    FILE* fd;
    
    int len = strlen(str);
    const char *last_four = &str[len-4];
    char txt[] = ".txt";
    
    if ( strcmp(last_four,txt) == 0)
    {
        printf("Failas buvo sukurtas.\n");
        fd = fopen (str, "w");
        fprintf(fd, "%d", x);
    }
    else {
        while(1){
            printf("Failas buvo pavadintas neteisingai.\n");
            printf("Iveskite failo pavadimima:\n");
            scanf ("%199s",str);
            len = strlen(str);
            last_four = &str[len-4];
            if(strcmp(last_four,txt) == 0) break;
        }
        fd = fopen (str, "w");
        fprintf(fd, "%d", x);
    }
    
    fclose(fd);
    
    return 0;
}
