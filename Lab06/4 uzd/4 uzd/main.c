//  Created by Mantas Kemėšius  on 23/10/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


int main(){
    FILE *fp;
    int y, m, d, ly, lm, ld, weekday = 0;
    char br1[1], br2[1], day[256];
    
    fp=fopen("week.txt", "r");
    
    printf("Iveskite data (Pavizdys: YYYY-MM-DD): \n");
    scanf("%d", &y);
    scanf("%c", &br1[0]);
    scanf("%d", &m);
    scanf("%c", &br2[0]);
    scanf("%d", &d);
    
    if (br1[0] == '-' && br2[0] == '-' && m <= 12 && d <= 31){
        weekday  = (d+=m<3?y--:y-2,23*m/9+d+4+y/4-y/100+y/400)%7;
        for( int i = 1; i <= 7; i++)
        {
            fscanf(fp, "%s", day);
            if(i == weekday) break;
        }
        printf("%s\n", day);
    }
    else {
        printf ("Data buvo ivesta, ne kaip pavizdyje.\n");
    }
    
    return 0;
}
