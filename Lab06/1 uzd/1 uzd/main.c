//  Created by Mantas Kemėšius  on 23/10/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.
#include <stdio.h>

int main() {
    
    int n, sum = 1;
    
    FILE* fr = fopen ("out.txt", "w");
    
    printf("Iveskite teigiama skaiciu ir gausite sio skaiciaus faktoriala:\n");
    scanf("%d", &n);
    
    for ( int i = 1 ; i <= n; i++)
    {
        sum *= i;
    }
    
    fprintf(fr, "Faktorialas yra: %d\n", sum);
    
    fclose(fr);
    
    return 0;
}
