//  Created by Mantas Kemėšius  on 23/10/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <stdlib.h>
int main()
{
    double a;
    int len=0, tarp, yra;
    
    printf("Parasykite realu skaiciu nuo 10 iki 1000 ir skaicius turi buti su 3 skaiciais po kablelio.\n");
    scanf("%lf", &a);
    
    FILE *fp = fopen("in.txt", "w");
    fprintf(fp, "%.3lf\n", a);
    fclose(fp);
    //---------------------
    //Reading what you wrote in the file
    
    fp = fopen("in.txt", "r");
    fscanf(fp, "%lf", &a);
    
    
    
    if(a > 10 && a < 1000)
    {
        printf("Jusu ivestas skaicius su 3 skaitmenimis po kablelio  : %.3lf\n", a);
        tarp = a;
        while(tarp != 0)
        {
            tarp = tarp/10;
            len++;
        }
        len = len + 3;
        printf("%.3lf skaiciaus ilgis: %d\n\n", a, len);
        fclose(fp);

    }
    else
    {
        printf("Jusu ivestas skaicius nera tarp 10 ir 1000\n");
        while(1)
        {
            printf("Iveskite realu skaiciu nuo 10 iki 1000:\n");
            scanf("%lf", &a);
            if(a > 10 && a < 1000)
            {
                yra = 1;
                break;
            }
        }
        if ( yra == 1){
            //for print
            fp = fopen("in.txt", "w");
            fprintf(fp, "%.3lf\n", a);
            fclose(fp);
            //for read
            fp = fopen("in.txt", "r");
            fscanf(fp, "%lf", &a);
            
            printf("Jusu ivestas skaicius su 3 skaitmenimis po kablelio  : %.3lf\n", a);
            tarp = a;
            while(tarp != 0)
            {
                tarp = tarp/10;
                len++;
            }
            len = len + 3;
            printf("%.3lf skaiciaus ilgis: %d\n\n", a, len);
            fclose(fp);
        }

    }
    
    return 0;
}
