//  Created by Mantas Kemėšius  on 15/10/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>

int main() {
   
    int n, m, tarp = 1, yra = 0, count = 0;
    char Board[200][200];
    int B[200][200];
    
    FILE* fd = fopen ("input.txt", "r");
    //FILE* fr = fopen ("rez.txt", "w");
    
    fscanf(fd, "%d%d", &m, &n);
    
    while(!feof (fd))
    {
        //READING---------------------------
        for ( int i = 0 ; i < m ; i++ )
        {
            for ( int j = 0 ; j <= n; j++)
            {
                fscanf(fd,"%c", &Board[i][j]);
            }
        }
        //-----------------------------------
    
        //CHANGING DOTS TO 0-------------------
        for ( int i = 0 ; i < m ; i++ )
        {
            for ( int j = 0 ; j <= n; j++)
            {
                if(Board[i][j] == '.') B[i][j] = 0;
                if(Board[i][j] == '*') B[i][j] = 69;
            }
        }
        //-------------------------------------
    
        //ADDING NUMBER OF MINES---------------
        for ( int i = 0 ; i < m ; i++ )
        {
            for ( int j = 1 ; j <= n; j++ )
            {
                if(B[i][j] != 69)
                {
                    //desne
                    if(B[i][j+1] == 69 && j != n)
                    {
                        B[i][j]++;
                    }
                    //kaire
                    if(j != 0 && B[i][j-1] == 69)
                    {
                        B[i][j]++;
                    }
                    //virsus
                    if(i!= 0 && B[i-1][j] == 69)
                    {
                        for ( int z = 1 ; z <= n; z++ )
                        {
                            if (B[i-1][z] == 69)
                            {
                                count++;
                            }
                        }
                        B[i][j]+= count;
                        count = 0;
                    }
                    //apacia
                    if (i != m-1 && B[i+1][j] == 69)
                    {
                        for ( int z = 1 ; z <= n; z++ )
                        {
                            if (B[i+1][z] == 69)
                            {
                                count++;
                            }
                        }
                        B[i][j]+=count;
                        count = 0;
                    }
                }
            }
        }
        //------------------------------------
    
        //ADDING NUMBER OF SQUARES---------------
        for ( int i = 0 ; i < m ; ++i )
        {
            for ( int j = 1 ; j <= n; ++j)
            {
                if(B[i][j] != 69)
                {
                    //top
                    if(i!= 0 && B[i-1][j] > 1 && B[i-1][j] != 69)
                    {
                        for ( int z = 1 ; z <= n; z++ )
                        {
                            if (B[i-1][z] == 69)
                            {
                                yra = 1;
                            }
                        }
                    
                        if (yra == 1)
                        {
                            B[i][j]++;
                            yra = 0;
                        }
                    }
                    //bottom
                    if (i != m-1 && B[i+1][j] > 1 && B[i+1][j] != 69)
                    {
                        for ( int z = 1 ; z <= n; z++ )
                        {
                            if (B[i+1][z] == 69)
                            {
                                yra = 1;
                            }
                        }
                        if (yra == 1)
                        {
                            B[i][j]++;
                            yra = 0;
                            count = 0;
                        }
                    }
                    //--
                }
            }
        }
        //-----------------------------------
    
        if ( m > 0 && n > 0 ) printf("Field #%d\n", tarp);
    
        for ( int i = 0 ; i < m ; i++ )
        {
            for ( int j = 1 ; j <= n; j++)
            {
                if (B[i][j] == 69) printf("* ");
                else printf("%d ", B[i][j]);
            }
            printf("\n");
        }
    
        tarp++;
        fscanf(fd, "%d%d", &m, &n);
        
        printf("\n");
    }
    
    fclose(fd);
    //fclose(fr);
    
return 0;
}
