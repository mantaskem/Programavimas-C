//  Created by Mantas Kemėšius  on 09/10/16.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <math.h>

int main() {
    
    int a, b, c;
    int t1 = 0, t2 = 1, nextTerm = 0, t = 0;
    
    printf("Iveskite tris neneigiamus sveikus skaicius a, b, c ir programa atspausdins fc.\n");
    
    scanf("%d%d%d",&a, &b, &c);
    
    if( a > 0 && b > 0 && c > 0){
        t1 = a;
        t2 = b;
    
        printf("Fibonacio seka:\n");
    
        printf("%d, %d, ", t1, t2);
        for (int i = 1; i <= c-2; i++)
        {
            t = t1;
            nextTerm = t1 + t2;
            t1 = t2;
            t2 = nextTerm;
            printf("%d, ", nextTerm);
        }
    
        printf("\n\n");
        
        if ( a == 0 && b == 1){
            printf("a=0 ir b=1, fc yra c-asis fibonacio sekos skaicius, taigi fc = %d.\n\n", t2);
        }
        else{
            printf("a!=0 ir b!=1, todel fc = fc-1+fc-2, taigi fc = %d.\n\n", t+t1);
        }
    }
    else printf("Ivedete neigiama skaiciu.\n");
    
    
    return 0;
}
