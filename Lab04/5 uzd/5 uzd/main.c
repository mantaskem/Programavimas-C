//  Created by Mantas Kemėšius  on 09/10/16.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <math.h>

int main() {
    
    int n;
    float vid, sum = 0, sk, max = -9999, min = 9999, neig = 0;
    
    printf("Iveskite n naturaluji skaiciu ir toliau veskite tiek naturaliuju skaiciu kiek yra n, programa isves suma, vidurkis, didziausia ir maziausia skaicius.\n");
    
    scanf("%d",&n);
    
    printf ("Iveskite %d skaicius: ", n);
    
    for ( int i = 0 ; i < n; i++){
        scanf("%f",&sk);
        sum += sk;
        if ( max < sk ) max = sk;
        if ( min > sk) min = sk;
        if ( sk < 0) neig = 1;
    }
    
    vid = sum / n;
    
    if ( neig == 0 )printf ("Suma yra: %.3f, vidurkis yra: %.3f, didziausias skaicius yra: %.3f, maziausias: %.3f\n", sum, vid, max, min);
    else printf ("Ivedete ne naturalu skaiciu.\n");
    
    return 0;
}
