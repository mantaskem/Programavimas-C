//  Created by Mantas Kemėšius  on 09/10/16.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <math.h>

int main() {
    
    long long int n, end, count = 0, max = -999, MaxSk = 0, sk;
    
    printf("Iveskite teigiamu skaiciu seka ir noredami ja uzbaigti parasykite pirma, bet neigiama skaiciu\n");
    
    scanf("%lld",&n);
    
    if ( n > 0){
        end = n * (-1);
    
        while (end != n){
            sk = n;
            count = 0;
            while(n != 0)
            {
                n /= 10;
                count++;
            }
            if ( max < count) {
                max = count;
                MaxSk = sk;
            }
            scanf("%lld",&n);
        }
    
        printf("Skaicius su daugiausia skaitmenu yra: %lld\n", MaxSk);
    }
    else printf("Pirmas skaicius buvo neigiamas.\n");
    
    
    return 0;
}
