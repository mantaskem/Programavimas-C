//  Created by Mantas Kemėšius  on 09/10/16.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <math.h>

int main() {
    
    int a, b, c, count = 0;
    
    printf("Iveskite tris sveikus skaicius a, b, c is intervalo (a; b], surasim skaicius kurie dalijasi iš skaiciaus c su liekana 1.\n");
    
    scanf("%d%d%d",&a, &b, &c);
    
    printf("Skaiciai kurie padalinti is c su liekana 1.\n");
    
    for (int i = a+1; i <= b; i++ ){
        if ( i % c == 1 && i > 0) {
            printf("%d ", i);
            count++;
        }
    }
    if (count == 0 ) printf ("Visi skaiciu sekoje esantys skaicia buvo neigiami.");
    printf("\n");
    
    
    return 0;
}
