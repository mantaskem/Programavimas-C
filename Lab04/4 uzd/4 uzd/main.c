//  Created by Mantas Kemėšius  on 09/10/16.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>

int main()
{
    int a, b, c, DBD = 0, MBK;
    
    printf("Iveskite tris sveikus skaicius a, b, c ir programa atspausdins DBD bei MBK\n");
    
    scanf("%d%d%d",&a, &b, &c);
    
    for ( int i=1; i <= a && i <= b && i <= c; ++i)
    {
        if( a % i == 0 && b % i == 0 && c % i == 0)
            DBD = i;
    }
    MBK = (a*b*c)/DBD;
    
    printf("DBD yra: %d ir MBK yra: %d\n", DBD, MBK);
    
return 0;
}
