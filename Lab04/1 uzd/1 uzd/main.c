//  Created by Mantas Kemėšius  on 09/10/16.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <math.h>

int main() {
    
    int a, b, c, how = 0;
    float D, x1, x2;
    
    printf("Iveskite tris sveikus skaicius a, b, c ir suskaiciuosime kiek lygtis: ax2+bx+c=0 turi sprendiniu bei kokie jie\n\n");
    
    scanf("%d%d%d",&a, &b, &c);
    
    D = (b*b) - (4*a*c);
    
    
    printf ("\n");
    
    if ( D >= 0)
    {
        D = sqrt(D);
        
        x1 = ((-1)*b + D)/(2*a);
        x2 = ((-1)*b - D)/(2*a);
        
        if(x1 != x2)
        {
            how += 2;
            printf ("Lygtis turi sprendinius: %d\nIr jie yra %f bei %f\n", how, x1, x2);
            
        }
        else
        {
            how++;
            printf ("Lygtis turi sprendiniu: %d\nIr jie yra %f\n", how, x1);
        }
        
    }
    else printf ("Nekorektiskai pagal salyga ivesti skaiciai arba lygybe neturi sprendiniu\n");
    
    
    return 0;
}
