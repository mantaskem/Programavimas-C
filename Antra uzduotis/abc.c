#include <stdio.h>
#include <stdbool.h>
int main()
{
    FILE* fd = fopen ("input.txt", "r");
    FILE* fr = fopen("output.txt", "w");

    int how = 0, pols, si ;
    int n, remainder = 0, i;
    int sk[200],j, kiek = 0, liekana, tarp = 0;
    int integer, vidurys;

    fscanf(fd,"%d%d", &si, &pols);

    while (!feof (fd)){// ciklas iki failo galo
        fprintf(fr,"%d %d\n", si, pols);
        while(pols != how){
            n = tarp;
            integer = 0;
            while ( n != 0){ //verciam i kita sistema
                liekana = n % si;
                n = n / si;
                sk[kiek] = liekana;
                kiek++;
            }
            kiek--;
            vidurys = kiek / 2;
            for ( int i = 0 ; i < kiek; i++){
                if(sk[i]==sk[kiek-i]){
                    remainder = 1;
                }
                else{
                    remainder = 0;
                    break;
                }
            }
            //---------------------------------------------------
            if (remainder == 1 && kiek > 0){
                for (int i = kiek; i >=0 ; i--){
                    if (sk[i] < 10) fprintf(fr,"%d", sk[i]);
                    else{
                        switch(sk[i]){
                            case 10:
                                fprintf(fr,"A");
                                break;
                            case 11:
                                fprintf(fr,"B");
                                break;
                            case 12:
                                fprintf(fr,"C");
                                break;
                            case 13:
                                fprintf(fr,"D");
                                break;
                            case 14:
                                fprintf(fr,"E");
                                break;
                            case 15:
                                fprintf(fr,"F");
                                break;
                            case 16:
                                fprintf(fr,"G");
                                break;
                            case 17:
                                fprintf(fr,"H");
                                break;
                            case 18:
                                fprintf(fr,"J");
                                break;
                            case 19:
                                fprintf(fr,"K");
                                break;
                        }
                    }
                }
                fprintf(fr, "\n");
                how++;
            }
            //----------------------------------------------------
            kiek = 0;
            tarp++;
        }
        tarp =0;
        how = 0;
        fprintf(fr,"---------------------\n");
        fscanf(fd,"%d%d", &si, &pols);
    }



    fclose(fd);
    fclose(fr);

return 0;
}
