//  Created by Mantas Kemėšius  on 17/12/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.

#include <stdio.h>
#include <string.h>
#include <assert.h>

void Str_cpy (char str[], char str2[]);
void Str_cat (char str[], char str2[]);
int Length (char str[], char str2[]);
int long Search(char str[]);
void Memcpy (char str[], char str2[], int *tarp1);
int long  Strc_spn (char str[], char str2[]);
void Str_tok (char str[], int *tarp1, int *tarp2);

int main() {
    
    int len = 0, KMemcmp, *tarp1, *tarp2, Before, After;
    int long where = 0, len2;
    char str[100], str2[100];
    
    Str_cpy(str, str2);
    
    assert(str != NULL && str2 != NULL);
    printf("Two strings is: |%s| and |%s|, after strcpy\n\n",str, str2);
    
    len = Length(str, str2);

    assert(len >= 10);
    
    printf("And these two string length is: |%d|, after strlen\n\n", len);
    
    Str_cat (str, str2);
    
    assert(str != NULL);
    
    printf("One united string is: |%s|, after: strcat \n\n", str);
    
    where = Search(str);
    
    assert(where > 1);
    
    printf("Index of first 't' is %ld. After strchr\n\n", where);
    
    Memcpy (str, str2, &KMemcmp);
    
    assert(strcmp(str, str2) != 0 && KMemcmp <= 0);
    
    printf("Two strings is not eqaul after memcpy and memset. Str: %s\nAnd Str2 is: %s\n\n", str, str2 );
    printf("Str is less than Str2 after memcmp, because %d < 0\n\n", KMemcmp);
    
    len2 = Strc_spn(str, str2);
    
    assert(len2 >= 9);
    
    printf("First match must be after 9 elements because we changed first string!\nMatch will be from %ld element.\n\n", len2);
    
    Str_tok (str, &Before, &After);
    
    assert(Before > After);
    printf("After deleting 10 elements from string with STRTOK\nCompering: Before deleting > After delete (%d > %d)\n",Before, After);
    
    
    return 0;
}
void Str_cpy (char str[], char str2[])
{
    strcpy(str,  "Unit test is really ");
    strcpy(str2, "Coooooooool!!");
}
int Length (char str[], char str2[])
{
    int temp = 0;
    
    temp += strlen(str);
    temp += strlen(str2);
    
    return temp;
}
void Str_cat (char str[], char str2[])
{
     strcat(str, str2);
}
int long Search(char str[])
{
    int long *tarp;
    char *pch=strchr(str,'t');
    while (pch!=NULL)
    {
        tarp = pch-str+1;
        break;
    }
    
    return tarp;
    
}
void Memcpy (char str[], char str2[], int *tarp1)
{
    memcpy(str2, str, strlen(str)+1);
    memset(str,'$',10);
    *tarp1 = memcmp(str, str2, 9);
}
int long Strc_spn (char str[], char str2[])
{
    int long len2 = strcspn(str, str2);
    
    return len2;
    
}
void Str_tok (char str[], int *tarp1, int *tarp2)
{
    const char s[2] = "$";
    
    *tarp1 = strlen(str);
    
    str = strtok(str, s);
    
    *tarp2 = strlen(str);
}
