//  Created by Mantas Kemėšius  on 30/10/2016.
//  Copyright © 2016 Mantas Kemėšius . All rights reserved.
//

#define False (1==0)
#define True  (1==1)

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
// ----------------------== FUNKCIJOS ==---------------------------------------------------------

int isInRange(int number, int low, int high);//Pirma uzduotis
int getFactorial(int number);//Antra uzduotis
int getFactorial2(int number);//Antra uzduotis IDOMU
int getPositiveNumber(char *msg);//Trecia uzduotis
void generateArray(int data[], int size, int low, int high);//Ketvirta uzduotis
long getFileSize(char *fileName);//Penkta uzduotis
int showMenu(char *menuTitle, char *menuOptions[], int menuSize, char *inputMsg);//Sesta uzduotis

// ----------------------------------------------------------------------------------------------
int main() {
    
    int sk, data[200];
    char *menuOptions[100];
    //---1 ---
    
    //isInRange(0,0,0);
    
    //Pirmos uzduoties testavimas.
    //if (isInRange(0,0,0) == 'True') printf ("True\n");
    //else printf ("False\n");
    //------------------------------------------------
    
    //---2 ---
    //printf("Iveskite skaiciu, kurio faktoriala noretumete grazinti:\n");
    //scanf("%d", &sk);
    //getFactorial(sk);
    
    //Antros uzduoties testavimas.
    //printf ("Faktorialas: %d\n", getFactorial(sk));
    //------------------------------------------------
    
    //---2 IDOMU---
    //getFactorial2(0);
    
    //Antros uzduoties testavimas.
    //printf ("Faktorialas: %d\n", getFactorial2(0));
    //------------------------------------------------
    
    //---3 ---
    
    //getPositiveNumber(0);
    
    
    //------------------------------------------------
    
    //--- 4 ---
    
    //generateArray(data,0,0,0);
    
    //------------------------------------------------
    
    //---5 ---
    
    //getFileSize(0);
    
    //------------------------------------------------
    
    //---6 ---
    
    showMenu(0,menuOptions,0,0);
    
    //------------------------------------------------
    
    
return 0;
}
// ----------------------== FUNKCIJOS ==---------------------------------------------------------
int isInRange(int number, int low, int high)//Pirma uzduotis
{
    int tarp;
    
    printf("Iveskite skaiciu, kuri norite patikrinti intervale:\n");
    scanf("%d", &number);
    printf("Nurodykite intervala:\n");
    printf("Nuo: ");
    scanf("%d", &low);
    printf("Iki: ");
    scanf("%d", &high);
    printf("Jusu skaicius: %d ir intervalas: [ %d ; %d ]\n", number, low, high);
    
    if( low > high )
    {
        if (number >= low && number <= high){
            tarp = 1;
        }
        else
        {
            tarp = 0;
        }
    }
    else tarp = 0;
    
    return tarp;
}
int getFactorial(int number)//Antra uzduotis
{
    
    if (number >= 1)
    {
        return number*getFactorial(number-1);
    }
    else
    {
        return 1; //Negaliu irasyti reiksmes 0, kadangi programa tada nebeveikia..
    }

}
int getFactorial2(int number)
{
    printf("Iveskite skaiciu, kurio faktoriala noretumete grazinti:\n");
    scanf("%d", &number);
    
    int sum = 1;
    
    if ( number >= 1 ){
        for ( int i = 0 ; i < number ; i++ )
        {
            sum *= number;
            number--;
        }
        return sum;
    }
    else
        return 0;
}
int getPositiveNumber(char *msg)//Trecia uzduotis
{
    long sk;
    
    msg = "Irasykite 0, jog uzbaigtumete programa.";
    char *p = msg;
     
    while (*p) {
        if (isdigit(*p)) {
            long val = strtol(p, &p, 10);
            printf ("%s\n", msg);
            scanf("%ld", &sk);
            if ( sk == val) {
                break;
            }
            else {
                while (1)
                {
                    printf ("%s\n", msg);
                    scanf("%ld", &sk);
                    if ( sk == val) {
                        break;
                    }
                }
            }
        }
        else {
            p++;
        }
    }
    
    return sk;
}
void generateArray(int data[], int size, int low, int high)//Ketvirta uzduotis
{
    
    int kiek = 0;
    
    printf("Iveskite masyvo ilgi: ");
    scanf("%d", &size);
    printf("\nIveskite intervala: ");
    printf("\nNuo: ");
    scanf("%d", &low);
    printf("\nIki: ");
    scanf("%d", &high);
    
    int r;
    srand(time(NULL));
    for ( int i = 0 ; i < size; i++ ){
        r = rand() % (high + 1 - low) + low;
        data[kiek] = r;
        //printf("%d\n", data[kiek]);
        kiek++;
    }
}
long getFileSize(char *fileName)//Penkta uzduotis
{
    FILE *size;
    long sz;
    
    fileName = "check.txt";
    
    size=fopen(fileName,"rb");
    if (!size)
    {
        printf("Tokio failo nera!");
        return -1;
    }
    else{
        fseek(size, 0L, SEEK_END);
        sz = ftell(size);
        //printf("Size - %ld bytes\n", sz);
        return sz;
    }
}
int showMenu(char *menuTitle, char *menuOptions[], int menuSize, char *inputMsg)//Sesta uzduotis
{
    int kiek = 1, whichOne = 0;
    
    menuTitle = "Crazy Stuffs";
    menuOptions[0] = "Home";
    menuOptions[1] = "New";
    menuOptions[2] = "About us";
    menuOptions[3] = "Our work";
    menuOptions[4] = "Our team";
    menuOptions[5] = "Why we";
    menuSize = 6;
    inputMsg = "Choose the option from (1 - 6): ";
    
    while (1){
    
        printf("%s\n", menuTitle);
        printf("Menu options:\n");
        for ( int i = 0 ; i < menuSize; i++)
        {
            printf("%s = %d\n", menuOptions[i], kiek);
            kiek++;
        }
        kiek = 1;
        printf("\n%s", inputMsg);
        scanf("%d", &whichOne);
        
        
        if (whichOne >= 1 && whichOne <= menuSize)
        {
            break;
        }
        else
        {
            continue;
        }
    }
    //---------Testavimas
    
    //whichOne--;
    //printf("\n%s\n", menuOptions[whichOne]);
    //whichOne++;
    
    return whichOne;
}
// ----------------------------------------------------------------------------------------------
